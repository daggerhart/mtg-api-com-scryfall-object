<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Endpoint\HttpEndpoint;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PhpExtended\Uuid\UuidParser;
use PhpMtg\Scryfall\ScryfallApiCard;
use PhpMtg\Scryfall\ScryfallApiEndpoint;
use PhpMtg\Scryfall\ScryfallApiExtension;
use PhpMtg\Scryfall\ScryfallApiRuling;
use PhpMtg\Scryfall\ScryfallApiSymbol;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ScryfallApiEndpointTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiEndpoint
 *
 * @internal
 *
 * @small
 */
class ScryfallApiEndpointTest extends TestCase
{
	
	/**
	 * The endpoint to test.
	 *
	 * @var ScryfallApiEndpoint
	 */
	protected $_endpoint;
	
	public function testGetSets() : void
	{
		foreach($this->_endpoint->getSets() as $set)
		{
			$this->assertInstanceOf(ScryfallApiExtension::class, $set);
		}
	}
	
	public function testGetSetByCode() : void
	{
		$set = $this->_endpoint->getSetByCode('tsr'); // Time Spiral Remastered
		
		$this->assertInstanceOf(ScryfallApiExtension::class, $set);
	}
	
	public function testGetSetByTcgplayerId() : void
	{
		$set = $this->_endpoint->getSetByTcgplayerId(2699); // Commander Collection: Green
		
		$this->assertInstanceOf(ScryfallApiExtension::class, $set);
	}
	
	public function testGetSetByScryfallId() : void
	{
		$set = $this->_endpoint->getSetByScryfallId((new UuidParser())->parse('39de6fbf-1f11-48d0-8f04-f0407f6a0732')); // Commander Legends
		
		$this->assertInstanceOf(ScryfallApiExtension::class, $set);
	}
	
	public function testGetCardsSearch() : void
	{
		$cards = $this->_endpoint->getCardsSearch(1, 'Jace', 'cards', 'name', 'auto', true, true, true);
		
		foreach($cards->getData() as $card)
		{
			$this->assertInstanceOf(ScryfallApiCard::class, $card);
		}
	}
	
	public function testGetCardNamed() : void
	{
		$cards = $this->_endpoint->getCardNamed('Jace, the mind Sculptor', true, 'wwk');
		
		$this->assertInstanceOf(ScryfallApiCard::class, $cards);
	}
	
	public function testGetCardsAutocomplete() : void
	{
		$cards = $this->_endpoint->getCardsAutocomplete('gob', true);
		
		foreach($cards->getData() as $card)
		{
			$this->assertIsString($card);
		}
	}
	
	public function testGetCardRandom() : void
	{
		$card = $this->_endpoint->getCardRandom('gob');
		
		$this->assertInstanceOf(ScryfallApiCard::class, $card);
	}
	
	public function testGetCard() : void
	{
		$card = $this->_endpoint->getCard('iko', '26', 'it'); // Patagia Tiger
		
		$this->assertInstanceOf(ScryfallApiCard::class, $card);
	}
	
	public function testGetCardByMultiverseId() : void
	{
		$card = $this->_endpoint->getCardByMultiverseId(479537); // Imposing Vantasaur
		
		$this->assertInstanceOf(ScryfallApiCard::class, $card);
	}
	
	public function testGetCardByMtgoId() : void
	{
		$card = $this->_endpoint->getCardByMtgoId(80011); // Adaptive Shimmerer
		
		$this->assertInstanceOf(ScryfallApiCard::class, $card);
	}
	
	public function testGetCardByArenaId() : void
	{
		$card = $this->_endpoint->getCardByArenaId(75443); // Angelic Guardian
		
		$this->assertInstanceOf(ScryfallApiCard::class, $card);
	}
	
	public function testGetCardByTcgplayerId() : void
	{
		$card = $this->_endpoint->getCardByTcgplayerId(193821); // God-Eternal Kefnet
		
		$this->assertInstanceOf(ScryfallApiCard::class, $card);
	}
	
	public function testGetCardByScryfallId() : void
	{
		$card = $this->_endpoint->getCardByScryfallId((new UuidParser())->parse('5b13ba5a-f4b0-420a-9e4f-a65e57721fa4')); // God-Eternal Oketra
		
		$this->assertInstanceOf(ScryfallApiCard::class, $card);
	}
	
	public function testGetCardByCardmarketId() : void
	{
		$card = $this->_endpoint->getCardByCardmarketId(452958); // Unbreakable Bond
		
		$this->assertInstanceOf(ScryfallApiCard::class, $card);
	}
	
	public function testGetCardImage() : void
	{
		$image = $this->_endpoint->getCardImage((new UuidParser())->parse('11568cdf-6148-494c-8b98-f5ca5797d775'), 'large', true);
		
		$this->assertIsString($image);
		$this->assertNotEmpty($image);
	}
	
	public function testRulingsFromMultiverseId() : void
	{
		$rulings = $this->_endpoint->getRulingsFromMultiverseId(491711); // Silundi Vision // Silundi Isle
		
		foreach($rulings->getData() as $ruling)
		{
			$this->assertInstanceOf(ScryfallApiRuling::class, $ruling);
		}
	}
	
	public function testRulingsFromMtgoId() : void
	{
		$rulings = $this->_endpoint->getRulingsFromMtgoId(83063); // Expedition Diviner
		
		foreach($rulings->getData() as $ruling)
		{
			$this->assertInstanceOf(ScryfallApiRuling::class, $ruling);
		}
	}
	
	public function testRulingsFromArenaId() : void
	{
		$rulings = $this->_endpoint->getRulingsFromArenaId(73183); // allied assault
		
		foreach($rulings->getData() as $ruling)
		{
			$this->assertInstanceOf(ScryfallApiRuling::class, $ruling);
		}
	}
	
	public function testRulingsFromCard() : void
	{
		$rulings = $this->_endpoint->getRulingsFromCard('cc1', '8'); // Command Tower
		
		foreach($rulings->getData() as $ruling)
		{
			$this->assertInstanceOf(ScryfallApiRuling::class, $ruling);
		}
	}
	
	public function testRulingsFromScryfallId() : void
	{
		$rulings = $this->_endpoint->getRulingsFromScryfallId((new UuidParser())->parse('4ce5b167-df7a-499e-8dcc-7aec2e28b382')); // Omnath locus of mana
		
		foreach($rulings->getData() as $ruling)
		{
			$this->assertInstanceOf(ScryfallApiRuling::class, $ruling);
		}
	}
	
	public function testSymbology() : void
	{
		$symbology = $this->_endpoint->getSymbology();
		
		foreach($symbology->getData() as $symbol)
		{
			$this->assertInstanceOf(ScryfallApiSymbol::class, $symbol);
		}
	}
	
	public function testParsedMana() : void
	{
		$parsedMana = $this->_endpoint->getParsedMana('RRRUUBG{B/G}');
		
		$this->assertTrue($parsedMana->isMulticolored());
		$this->assertEquals('{B/G}{U}{U}{B}{R}{R}{R}{G}', $parsedMana->getCost());
	}
	
	public function testCardNames() : void
	{
		$cardNames = $this->_endpoint->getCardNames();
		
		foreach($cardNames->getData() as $cardName)
		{
			$this->assertIsString($cardName);
		}
	}
	
	public function testArtistNames() : void
	{
		$artistNames = $this->_endpoint->getArtistNames();
		
		foreach($artistNames->getData() as $artistName)
		{
			$this->assertIsString($artistName);
		}
	}
	
	public function testWordBank() : void
	{
		$wordBank = $this->_endpoint->getWordBank();
		
		foreach($wordBank->getData() as $word)
		{
			$this->assertIsString($word);
		}
	}
	
	public function testCreatureTypes() : void
	{
		$creatureTypes = $this->_endpoint->getCreatureTypes();
		
		foreach($creatureTypes->getData() as $creatureType)
		{
			$this->assertIsString($creatureType);
		}
	}
	
	public function testPlaneswalkerTypes() : void
	{
		$planeswalkerTypes = $this->_endpoint->getPlaneswalkerTypes();
		
		foreach($planeswalkerTypes->getData() as $planeswalkerType)
		{
			$this->assertIsString($planeswalkerType);
		}
	}
	
	public function testLandTypes() : void
	{
		$landTypes = $this->_endpoint->getLandTypes();
		
		foreach($landTypes->getData() as $landType)
		{
			$this->assertIsString($landType);
		}
	}
	
	public function testArtifactTypes() : void
	{
		$artifactTypes = $this->_endpoint->getArtifactTypes();
		
		foreach($artifactTypes->getData() as $artifactType)
		{
			$this->assertIsString($artifactType);
		}
	}
	
	public function testGetEnchantmentTypes() : void
	{
		$enchantmentTypes = $this->_endpoint->getEnchantmentTypes();
		
		foreach($enchantmentTypes->getData() as $enchantmentType)
		{
			$this->assertIsString($enchantmentType);
		}
	}
	
	public function testGetSpellTypes() : void
	{
		$spellTypes = $this->_endpoint->getSpellTypes();
		
		foreach($spellTypes->getData() as $spellType)
		{
			$this->assertIsString($spellType);
		}
	}
	
	public function testGetPowers() : void
	{
		$powers = $this->_endpoint->getPowers();
		
		foreach($powers->getData() as $power)
		{
			$this->assertIsString($power);
		}
	}
	
	public function testGetToughnesses() : void
	{
		$toughnesses = $this->_endpoint->getToughnesses();
		
		foreach($toughnesses->getData() as $toughness)
		{
			$this->assertIsString($toughness);
		}
	}
	
	public function testGetLoyalties() : void
	{
		$loyalties = $this->_endpoint->getLoyalties();
		
		foreach($loyalties->getData() as $loyalty)
		{
			$this->assertIsString($loyalty);
		}
	}
	
	public function testGetWatermarks() : void
	{
		$watermarks = $this->_endpoint->getWatermarks();
		
		foreach($watermarks->getData() as $watermark)
		{
			$this->assertIsString($watermark);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface {
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				\sleep(1); // be kind and obey the delay they ask
// 				\var_dump($request->getUri()->__toString());
				$options = ['http' => ['user_agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0']];
				$data = \file_get_contents($request->getUri()->__toString(), false, \stream_context_create($options));
				$body = new StringStream($data);
				
				return (new Response())->withBody($body);
			}
		};
		
		$this->_endpoint = new ScryfallApiEndpoint(new HttpEndpoint($client));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_endpoint = null;
	}
	
}
