<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpMtg\Scryfall\ScryfallApiCardLegality;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiCardLegalityTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiCardLegality
 *
 * @internal
 *
 * @small
 */
class ScryfallApiCardLegalityTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiCardLegality
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL CARD LEGALITY', $this->_object->__toString());
	}
	
	public function testGet1v1() : void
	{
		$this->assertNull($this->_object->get1v1());
		$this->_object->set1v1('oneVone');
		$this->assertEquals('oneVone', $this->_object->get1v1());
	}
	
	public function testGetAlchemy() : void
	{
		$this->assertNull($this->_object->getAlchemy());
		$this->_object->setAlchemy('alchemy');
		$this->assertEquals('alchemy', $this->_object->getAlchemy());
	}
	
	public function testGetBrawl() : void
	{
		$this->assertNull($this->_object->getBrawl());
		$this->_object->setBrawl('brawl');
		$this->assertEquals('brawl', $this->_object->getBrawl());
	}
	
	public function testGetCommander() : void
	{
		$this->assertNull($this->_object->getCommander());
		$this->_object->setCommander('commander');
		$this->assertEquals('commander', $this->_object->getCommander());
	}
	
	public function testGetDuel() : void
	{
		$this->assertNull($this->_object->getDuel());
		$this->_object->setDuel('duel');
		$this->assertEquals('duel', $this->_object->getDuel());
	}
	
	public function testGetExplorer() : void
	{
		$this->assertNull($this->_object->getExplorer());
		$this->_object->setExplorer('explorer');
		$this->assertEquals('explorer', $this->_object->getExplorer());
	}
	
	public function testGetFrontier() : void
	{
		$this->assertNull($this->_object->getFrontier());
		$this->_object->setFrontier('frontier');
		$this->assertEquals('frontier', $this->_object->getFrontier());
	}
	
	public function testGetFuture() : void
	{
		$this->assertNull($this->_object->getFuture());
		$this->_object->setFuture('future');
		$this->assertEquals('future', $this->_object->getFuture());
	}
	
	public function testGetGladiator() : void
	{
		$this->assertNull($this->_object->getGladiator());
		$this->_object->setGladiator('gladiator');
		$this->assertEquals('gladiator', $this->_object->getGladiator());
	}
	
	public function testGetHistoric() : void
	{
		$this->assertNull($this->_object->getHistoric());
		$this->_object->setHistoric('historic');
		$this->assertEquals('historic', $this->_object->getHistoric());
	}
	
	public function testGetHistoricBrawl() : void
	{
		$this->assertNull($this->_object->getHistoricBrawl());
		$this->_object->setHistoricBrawl('historicBrawl');
		$this->assertEquals('historicBrawl', $this->_object->getHistoricBrawl());
	}
	
	public function testGetLegacy() : void
	{
		$this->assertNull($this->_object->getLegacy());
		$this->_object->setLegacy('legacy');
		$this->assertEquals('legacy', $this->_object->getLegacy());
	}
	
	public function testGetModern() : void
	{
		$this->assertNull($this->_object->getModern());
		$this->_object->setModern('modern');
		$this->assertEquals('modern', $this->_object->getModern());
	}
	
	public function testGetOldschool() : void
	{
		$this->assertNull($this->_object->getOldSchool());
		$this->_object->setOldSchool('oldschool');
		$this->assertEquals('oldschool', $this->_object->getOldSchool());
	}
	
	public function testGetPauper() : void
	{
		$this->assertNull($this->_object->getPauper());
		$this->_object->setPauper('pauper');
		$this->assertEquals('pauper', $this->_object->getPauper());
	}
	
	public function testGetPauperCommander() : void
	{
		$this->assertNull($this->_object->getPauperCommander());
		$this->_object->setPauperCommander('pauperCommander');
		$this->assertEquals('pauperCommander', $this->_object->getPauperCommander());
	}
	
	public function testGetPenny() : void
	{
		$this->assertNull($this->_object->getPenny());
		$this->_object->setPenny('penny');
		$this->assertEquals('penny', $this->_object->getPenny());
	}
	
	public function testGetPioneer() : void
	{
		$this->assertNull($this->_object->getPioneer());
		$this->_object->setPioneer('pioneer');
		$this->assertEquals('pioneer', $this->_object->getPioneer());
	}
	
	public function testGetPremodern() : void
	{
		$this->assertNull($this->_object->getPremodern());
		$this->_object->setPremodern('premodern');
		$this->assertEquals('premodern', $this->_object->getPremodern());
	}
	
	public function testGetStandard() : void
	{
		$this->assertNull($this->_object->getStandard());
		$this->_object->setStandard('standard');
		$this->assertEquals('standard', $this->_object->getStandard());
	}
	
	public function testGetVintage() : void
	{
		$this->assertNull($this->_object->getVintage());
		$this->_object->setVintage('vintage');
		$this->assertEquals('vintage', $this->_object->getVintage());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiCardLegality();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
