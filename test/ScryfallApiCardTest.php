<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Uuid\Uuid;
use PhpMtg\Scryfall\ScryfallApiCard;
use PhpMtg\Scryfall\ScryfallApiCardFace;
use PhpMtg\Scryfall\ScryfallApiCardImagery;
use PhpMtg\Scryfall\ScryfallApiCardLegality;
use PhpMtg\Scryfall\ScryfallApiCardPurchase;
use PhpMtg\Scryfall\ScryfallApiCardResource;
use PhpMtg\Scryfall\ScryfallApiPreview;
use PhpMtg\Scryfall\ScryfallApiPrices;
use PhpMtg\Scryfall\ScryfallApiRelatedCard;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiCardTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiCard
 *
 * @internal
 *
 * @small
 */
class ScryfallApiCardTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiCard
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL CARD 12345678-1234-5678-1234-1234567890ab', $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab), $this->_object->getId());
	}
	
	public function testGetArenaId() : void
	{
		$this->assertNull($this->_object->getArenaId());
		$this->_object->setArenaId(123);
		$this->assertEquals(123, $this->_object->getArenaId());
	}
	
	public function testGetLang() : void
	{
		$this->assertNull($this->_object->getLang());
		$this->_object->setLang('en');
		$this->assertEquals('en', $this->_object->getLang());
	}
	
	public function testGetMtgoId() : void
	{
		$this->assertNull($this->_object->getMtgoId());
		$this->_object->setMtgoId(123);
		$this->assertEquals(123, $this->_object->getMtgoId());
	}
	
	public function testGetMtgoFoilId() : void
	{
		$this->assertNull($this->_object->getMtgoFoilId());
		$this->_object->setMtgoFoilId(123);
		$this->assertEquals(123, $this->_object->getMtgoFoilId());
	}
	
	public function testGetMultiverseIds() : void
	{
		$this->assertEquals([], $this->_object->getMultiverseIds());
		$this->_object->setMultiverseIds([123]);
		$this->assertEquals([123], $this->_object->getMultiverseIds());
	}
	
	public function testGetTcgplayerId() : void
	{
		$this->assertNull($this->_object->getTcgplayerId());
		$this->_object->setTcgplayerId(123);
		$this->assertEquals(123, $this->_object->getTcgplayerId());
	}
	
	public function testGetTcgplayerEtchedId() : void
	{
		$this->assertNull($this->_object->getTcgplayerEtchedId());
		$this->_object->setTcgplayerEtchedId(123);
		$this->assertEquals(123, $this->_object->getTcgplayerEtchedId());
	}
	
	public function testGetCardmarketId() : void
	{
		$this->assertNull($this->_object->getCardmarketId());
		$this->_object->setCardmarketId(123);
		$this->assertEquals(123, $this->_object->getCardmarketId());
	}
	
	public function testGetOracleId() : void
	{
		$this->assertNull($this->_object->getOracleId());
		$uuid = new Uuid(1, 2, 3, 4, 5, 6, 7);
		$this->_object->setOracleId($uuid);
		$this->assertEquals($uuid, $this->_object->getOracleId());
	}
	
	public function testSetPrintsSearchUri() : void
	{
		$this->assertNull($this->_object->getPrintsSearchUri());
		$uri = new Uri();
		$this->_object->setPrintsSearchUri($uri);
		$this->assertEquals($uri, $this->_object->getPrintsSearchUri());
	}
	
	public function testGetRulingsUri() : void
	{
		$this->assertNull($this->_object->getRulingsUri());
		$uri = new Uri();
		$this->_object->setRulingsUri($uri);
		$this->assertEquals($uri, $this->_object->getRulingsUri());
	}
	
	public function testGetScryfallUri() : void
	{
		$this->assertNull($this->_object->getScryfallUri());
		$uri = new Uri();
		$this->_object->setScryfallUri($uri);
		$this->assertEquals($uri, $this->_object->getScryfallUri());
	}
	
	public function testGetUri() : void
	{
		$this->assertNull($this->_object->getUri());
		$uri = new Uri();
		$this->_object->setUri($uri);
		$this->assertEquals($uri, $this->_object->getUri());
	}
	
	public function testGetAllParts() : void
	{
		$this->assertEquals(new ArrayIterator(), $this->_object->getAllParts());
		$parts = new ArrayIterator([new ScryfallApiRelatedCard(new Uuid(0, 0, 0, 0, 0, 0, 0))]);
		$this->_object->setAllParts($parts);
		$this->assertEquals($parts, $this->_object->getAllParts());
	}
	
	public function testGetCardFaces() : void
	{
		$this->assertEquals(new ArrayIterator(), $this->_object->getCardFaces());
		$faces = new ArrayIterator([new ScryfallApiCardFace()]);
		$this->_object->setCardFaces($faces);
		$this->assertEquals($faces, $this->_object->getCardFaces());
	}
	
	public function testGetCmc() : void
	{
		$this->assertNull($this->_object->getCmc());
		$this->_object->setCmc(2);
		$this->assertEquals(2, $this->_object->getCmc());
	}
	
	public function testGetColors() : void
	{
		$this->assertEquals([], $this->_object->getColors());
		$this->_object->setColors(['B']);
		$this->assertEquals(['B'], $this->_object->getColors());
	}
	
	public function testGetColorIdentity() : void
	{
		$this->assertEquals([], $this->_object->getColorIdentity());
		$this->_object->setColorIdentity(['B']);
		$this->assertEquals(['B'], $this->_object->getColorIdentity());
	}
	
	public function testGetColorIndicator() : void
	{
		$this->assertEquals([], $this->_object->getColorIndicator());
		$this->_object->setColorIndicator(['B']);
		$this->assertEquals(['B'], $this->_object->getColorIndicator());
	}
	
	public function testGetEdhrecRank() : void
	{
		$this->assertNull($this->_object->getEdhrecRank());
		$this->_object->setEdhrecRank(123);
		$this->assertEquals(123, $this->_object->getEdhrecRank());
	}
	
	public function testGetFoil() : void
	{
		$this->assertNull($this->_object->isFoil());
		$this->_object->setFoil(true);
		$this->assertTrue($this->_object->isFoil());
	}
	
	public function testGetFinishes() : void
	{
		$this->assertEquals([], $this->_object->getFinishes());
		$this->_object->setFinishes(['finish']);
		$this->assertEquals(['finish'], $this->_object->getFinishes());
	}
	
	public function testGetHandModifier() : void
	{
		$this->assertNull($this->_object->getHandModifier());
		$this->_object->setHandModifier('+10');
		$this->assertEquals('+10', $this->_object->getHandModifier());
	}
	
	public function testGetKeywords() : void
	{
		$this->assertEquals([], $this->_object->getKeywords());
		$this->_object->setKeywords(['keyword']);
		$this->assertEquals(['keyword'], $this->_object->getKeywords());
	}
	
	public function testGetProducedMana() : void
	{
		$this->assertEquals([], $this->_object->getProducedMana());
		$this->_object->setProducedMana(['B']);
		$this->assertEquals(['B'], $this->_object->getProducedMana());
	}
	
	public function testGetLayout() : void
	{
		$this->assertNull($this->_object->getLayout());
		$this->_object->setLayout('layout');
		$this->assertEquals('layout', $this->_object->getLayout());
	}
	
	public function testGetLegalities() : void
	{
		$this->assertNull($this->_object->getLegalities());
		$legality = new ScryfallApiCardLegality();
		$this->_object->setLegalities($legality);
		$this->assertEquals($legality, $this->_object->getLegalities());
	}
	
	public function testGetLifeModifier() : void
	{
		$this->assertNull($this->_object->getLifeModifier());
		$this->_object->setLifeModifier('+3');
		$this->assertEquals('+3', $this->_object->getLifeModifier());
	}
	
	public function testGetLoyalty() : void
	{
		$this->assertNull($this->_object->getLoyalty());
		$this->_object->setLoyalty('+5');
		$this->assertEquals('+5', $this->_object->getLoyalty());
	}
	
	public function testGetManaCost() : void
	{
		$this->assertNull($this->_object->getManaCost());
		$this->_object->setManaCost('BB');
		$this->assertEquals('BB', $this->_object->getManaCost());
	}
	
	public function testGetName() : void
	{
		$this->assertNull($this->_object->getName());
		$this->_object->setName('name');
		$this->assertEquals('name', $this->_object->getName());
	}
	
	public function testGetNonFoil() : void
	{
		$this->assertNull($this->_object->isNonfoil());
		$this->_object->setNonFoil(true);
		$this->assertTrue($this->_object->isNonfoil());
	}
	
	public function testGetOracleText() : void
	{
		$this->assertNull($this->_object->getOracleText());
		$this->_object->setOracleText('text');
		$this->assertEquals('text', $this->_object->getOracleText());
	}
	
	public function testGetOversized() : void
	{
		$this->assertNull($this->_object->isOversized());
		$this->_object->setOversized(true);
		$this->assertTrue($this->_object->isOversized());
	}
	
	public function testGetPennyRank() : void
	{
		$this->assertNull($this->_object->getPennyRank());
		$this->_object->setPennyRank(1);
		$this->assertEquals(1, $this->_object->getPennyRank());
	}
	
	public function testGetPower() : void
	{
		$this->assertNull($this->_object->getPower());
		$this->_object->setPower('+4');
		$this->assertEquals('+4', $this->_object->getPower());
	}
	
	public function testGetReserved() : void
	{
		$this->assertNull($this->_object->isReserved());
		$this->_object->setReserved(true);
		$this->assertTrue($this->_object->isReserved());
	}
	
	public function testGetSecurityStamp() : void
	{
		$this->assertNull($this->_object->getSecurityStamp());
		$this->_object->setSecurityStamp('acorn');
		$this->assertEquals('acorn', $this->_object->getSecurityStamp());
	}
	
	public function testGetToughness() : void
	{
		$this->assertNull($this->_object->getToughness());
		$this->_object->setToughness('+3');
		$this->assertEquals('+3', $this->_object->getToughness());
	}
	
	public function testGetTypeline() : void
	{
		$this->assertNull($this->_object->getTypeLine());
		$this->_object->setTypeline('typeline');
		$this->assertEquals('typeline', $this->_object->getTypeLine());
	}
	
	public function testGetArtist() : void
	{
		$this->assertNull($this->_object->getArtist());
		$this->_object->setArtist('artist');
		$this->assertEquals('artist', $this->_object->getArtist());
	}
	
	public function testGetArtistIds() : void
	{
		$this->assertEquals([], $this->_object->getArtistIds());
		$this->_object->setArtistIds(['artist']);
		$this->assertEquals(['artist'], $this->_object->getArtistIds());
	}
	
	public function testGetBooster() : void
	{
		$this->assertNull($this->_object->hasBooster());
		$this->_object->setBooster(true);
		$this->assertTrue($this->_object->hasBooster());
	}
	
	public function testGetBorderColor() : void
	{
		$this->assertNull($this->_object->getBorderColor());
		$this->_object->setBorderColor('B');
		$this->assertEquals('B', $this->_object->getBorderColor());
	}
	
	public function testGetCardBackId() : void
	{
		$this->assertNull($this->_object->getCardBackId());
		$uuid = new Uuid(1, 2, 3, 4, 5, 6, 7);
		$this->_object->setCardBackId($uuid);
		$this->assertEquals($uuid, $this->_object->getCardBackId());
	}
	
	public function testGetCollectorNumber() : void
	{
		$this->assertNull($this->_object->getCollectorNumber());
		$this->_object->setCollectorNumber('123*');
		$this->assertEquals('123*', $this->_object->getCollectorNumber());
	}
	
	public function testGetDigital() : void
	{
		$this->assertNull($this->_object->isDigital());
		$this->_object->setDigital(true);
		$this->assertTrue($this->_object->isDigital());
	}
	
	public function testGetFlavorText() : void
	{
		$this->assertNull($this->_object->getFlavorText());
		$this->_object->setFlavorText('flavortext');
		$this->assertEquals('flavortext', $this->_object->getFlavorText());
	}
	
	public function testGetFrameEffects() : void
	{
		$this->assertEquals([], $this->_object->getFrameEffects());
		$this->_object->setFrameEffects(['effect']);
		$this->assertEquals(['effect'], $this->_object->getFrameEffects());
	}
	
	public function testGetFrame() : void
	{
		$this->assertNull($this->_object->getFrame());
		$this->_object->setFrame('frame');
		$this->assertEquals('frame', $this->_object->getFrame());
	}
	
	public function testGetFullArt() : void
	{
		$this->assertNull($this->_object->isFullArt());
		$this->_object->setFullArt(true);
		$this->assertTrue($this->_object->isFullArt());
	}
	
	public function testGetGames() : void
	{
		$this->assertEquals([], $this->_object->getGames());
		$this->_object->setGames(['game']);
		$this->assertEquals(['game'], $this->_object->getGames());
	}
	
	public function testGetHighresImage() : void
	{
		$this->assertNull($this->_object->isHighresImage());
		$this->_object->setHighresImage(true);
		$this->assertTrue($this->_object->isHighresImage());
	}
	
	public function testGetImageStatus() : void
	{
		$this->assertNull($this->_object->getImageStatus());
		$this->_object->setImageStatus('status');
		$this->assertEquals('status', $this->_object->getImageStatus());
	}
	
	public function testGetIllustrationId() : void
	{
		$this->assertNull($this->_object->getIllustrationId());
		$uuid = new Uuid(1, 2, 3, 4, 5, 6, 7);
		$this->_object->setIllustrationId($uuid);
		$this->assertEquals($uuid, $this->_object->getIllustrationId());
	}
	
	public function testGetImageUris() : void
	{
		$this->assertNull($this->_object->getImageUris());
		$imagery = new ScryfallApiCardImagery();
		$this->_object->setImageUris($imagery);
		$this->assertEquals($imagery, $this->_object->getImageUris());
	}
	
	public function testGetPrintedName() : void
	{
		$this->assertNull($this->_object->getPrintedName());
		$this->_object->setPrintedName('name');
		$this->assertEquals('name', $this->_object->getPrintedName());
	}
	
	public function testGetPrintedText() : void
	{
		$this->assertNull($this->_object->getPrintedText());
		$this->_object->setPrintedText('text');
		$this->assertEquals('text', $this->_object->getPrintedText());
	}
	
	public function testGetPrintedTypeline() : void
	{
		$this->assertNull($this->_object->getPrintedTypeLine());
		$this->_object->setPrintedTypeLine('typeline');
		$this->assertEquals('typeline', $this->_object->getPrintedTypeLine());
	}
	
	public function testGetPromo() : void
	{
		$this->assertNull($this->_object->isPromo());
		$this->_object->setPromo(true);
		$this->assertTrue($this->_object->isPromo());
	}
	
	public function testGetPromoTypes() : void
	{
		$this->assertEquals([], $this->_object->getPromoTypes());
		$this->_object->setPromoTypes(['type']);
		$this->assertEquals(['type'], $this->_object->getPromoTypes());
	}
	
	public function testGetPurchaseUris() : void
	{
		$this->assertNull($this->_object->getPurchaseUris());
		$purchase = new ScryfallApiCardPurchase();
		$this->_object->setPurchaseUris($purchase);
		$this->assertEquals($purchase, $this->_object->getPurchaseUris());
	}
	
	public function testGetRarity() : void
	{
		$this->assertNull($this->_object->getRarity());
		$this->_object->setRarity('rarity');
		$this->assertEquals('rarity', $this->_object->getRarity());
	}
	
	public function testGetRelatedUris() : void
	{
		$this->assertNull($this->_object->getRelatedUris());
		$resources = new ScryfallApiCardResource();
		$this->_object->setRelatedUris($resources);
		$this->assertEquals($resources, $this->_object->getRelatedUris());
	}
	
	public function testGetPrices() : void
	{
		$this->assertNull($this->_object->getPrices());
		$prices = new ScryfallApiPrices();
		$this->_object->setPrices($prices);
		$this->assertEquals($prices, $this->_object->getPrices());
	}
	
	public function testGetRelatedAt() : void
	{
		$this->assertNull($this->_object->getReleasedAt());
		$datetime = new DateTimeImmutable();
		$this->_object->setReleasedAt($datetime);
		$this->assertEquals($datetime, $this->_object->getReleasedAt());
	}
	
	public function testGetReprint() : void
	{
		$this->assertNull($this->_object->isReprint());
		$this->_object->setReprint(true);
		$this->assertTrue($this->_object->isReprint());
	}
	
	public function testGetScryfallSetUri() : void
	{
		$this->assertNull($this->_object->getScryfallSetUri());
		$uri = new Uri();
		$this->_object->setScryfallSetUri($uri);
		$this->assertEquals($uri, $this->_object->getScryfallSetUri());
	}
	
	public function testGetSetId() : void
	{
		$this->assertNull($this->_object->getSetId());
		$this->_object->setSetId('setid');
		$this->assertEquals('setid', $this->_object->getSetId());
	}
	
	public function testGetSet() : void
	{
		$this->assertNull($this->_object->getSet());
		$this->_object->setSet('set');
		$this->assertEquals('set', $this->_object->getSet());
	}
	
	public function testGetSetName() : void
	{
		$this->assertNull($this->_object->getSetName());
		$this->_object->setSetName('name');
		$this->assertEquals('name', $this->_object->getSetName());
	}
	
	public function testGetSetSearchUri() : void
	{
		$this->assertNull($this->_object->getSetSearchUri());
		$uri = new Uri();
		$this->_object->setSetSearchUri($uri);
		$this->assertEquals($uri, $this->_object->getSetSearchUri());
	}
	
	public function testGetSetType() : void
	{
		$this->assertNull($this->_object->getSetType());
		$this->_object->setSetType('type');
		$this->assertEquals('type', $this->_object->getSetType());
	}
	
	public function testGetSetUri() : void
	{
		$this->assertNull($this->_object->getSetUri());
		$uri = new Uri();
		$this->_object->setSetUri($uri);
		$this->assertEquals($uri, $this->_object->getSetUri());
	}
	
	public function testGetStorySpotlight() : void
	{
		$this->assertNull($this->_object->isStorySpotlight());
		$this->_object->setStorySpotlight(true);
		$this->assertTrue($this->_object->isStorySpotlight());
	}
	
	public function testGetTextless() : void
	{
		$this->assertNull($this->_object->isTextless());
		$this->_object->setTextless(true);
		$this->assertTrue($this->_object->isTextless());
	}
	
	public function testGetVariation() : void
	{
		$this->assertNull($this->_object->isVariation());
		$this->_object->setVariation(true);
		$this->assertTrue($this->_object->isVariation());
	}
	
	public function testGetVariationOf() : void
	{
		$this->assertNull($this->_object->getVariationOf());
		$uuid = new Uuid(1, 2, 3, 4, 5, 6, 7);
		$this->_object->setVariationOf($uuid);
		$this->assertEquals($uuid, $this->_object->getVariationOf());
	}
	
	public function testGetWatermark() : void
	{
		$this->assertNull($this->_object->getWatermark());
		$this->_object->setWatermark('watermark');
		$this->assertEquals('watermark', $this->_object->getWatermark());
	}
	
	public function testGetPreview() : void
	{
		$this->assertNull($this->_object->getPreview());
		$preview = new ScryfallApiPreview();
		$this->_object->setPreview($preview);
		$this->assertEquals($preview, $this->_object->getPreview());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiCard(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
