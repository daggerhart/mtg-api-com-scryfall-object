<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpMtg\Scryfall\ScryfallApiPrices;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiPricesTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiPrices
 *
 * @internal
 *
 * @small
 */
class ScryfallApiPricesTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiPrices
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL API PRICE', $this->_object->__toString());
	}
	
	public function testGetUsd() : void
	{
		$this->assertNull($this->_object->getUsd());
		$this->_object->setUsd(2);
		$this->assertEquals(2, $this->_object->getUsd());
	}
	
	public function testGetUsdFoil() : void
	{
		$this->assertNull($this->_object->getUsdFoil());
		$this->_object->setUsdFoil(2);
		$this->assertEquals(2, $this->_object->getUsdFoil());
	}
	
	public function testGetUsdEtched() : void
	{
		$this->assertNull($this->_object->getUsdEtched());
		$this->_object->setUsdEtched(2);
		$this->assertEquals(2, $this->_object->getUsdEtched());
	}
	
	public function testGetUsdGlossy() : void
	{
		$this->assertNull($this->_object->getUsdGlossy());
		$this->_object->setUsdGlossy(2);
		$this->assertEquals(2, $this->_object->getUsdGlossy());
	}
	
	public function testGetEur() : void
	{
		$this->assertNull($this->_object->getEur());
		$this->_object->setEur(2);
		$this->assertEquals(2, $this->_object->getEur());
	}
	
	public function testGetEurFoil() : void
	{
		$this->assertNull($this->_object->getEurFoil());
		$this->_object->setEurFoil(2);
		$this->assertEquals(2, $this->_object->getEurFoil());
	}
	
	public function testGetTix() : void
	{
		$this->assertNull($this->_object->getTix());
		$this->_object->setTix(2);
		$this->assertEquals(2, $this->_object->getTix());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiPrices();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
