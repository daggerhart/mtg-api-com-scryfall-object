<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Uuid\Uuid;
use PhpMtg\Scryfall\ScryfallApiRelatedCard;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiRelatedCardTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiRelatedCard
 *
 * @internal
 *
 * @small
 */
class ScryfallApiRelatedCardTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiRelatedCard
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL RELATED CARD 12345678-1234-5678-1234-1234567890ab', $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab), $this->_object->getId());
	}
	
	public function testGetComponent() : void
	{
		$this->assertNull($this->_object->getComponent());
		$this->_object->setComponent('meld');
		$this->assertEquals('meld', $this->_object->getComponent());
	}
	
	public function testGetName() : void
	{
		$this->assertNull($this->_object->getName());
		$this->_object->setName('name');
		$this->assertEquals('name', $this->_object->getName());
	}
	
	public function testGetTypeline() : void
	{
		$this->assertNull($this->_object->getTypeLine());
		$this->_object->setTypeline('typeline');
		$this->assertEquals('typeline', $this->_object->getTypeLine());
	}
	
	public function testGetUri() : void
	{
		$this->assertNull($this->_object->getUri());
		$uri = new Uri();
		$this->_object->setUri($uri);
		$this->assertEquals($uri, $this->_object->getUri());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiRelatedCard(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
