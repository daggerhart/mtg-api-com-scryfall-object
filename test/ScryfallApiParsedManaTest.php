<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpMtg\Scryfall\ScryfallApiParsedMana;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiParsedManaTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiParsedMana
 *
 * @internal
 *
 * @small
 */
class ScryfallApiParsedManaTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiParsedMana
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL PARSED MANA', $this->_object->__toString());
	}
	
	public function testGetCost() : void
	{
		$this->assertNull($this->_object->getCost());
		$this->_object->setCost('cost');
		$this->assertEquals('cost', $this->_object->getCost());
	}
	
	public function testGetCmc() : void
	{
		$this->assertNull($this->_object->getCmc());
		$this->_object->setCmc(2);
		$this->assertEquals(2, $this->_object->getCmc());
	}
	
	public function testGetColors() : void
	{
		$this->assertEquals([], $this->_object->getColors());
		$this->_object->setColors(['B']);
		$this->assertEquals(['B'], $this->_object->getColors());
	}
	
	public function testGetColorless() : void
	{
		$this->assertNull($this->_object->isColorless());
		$this->_object->setColorless(true);
		$this->assertTrue($this->_object->isColorless());
	}
	
	public function testGetMonocolored() : void
	{
		$this->assertNull($this->_object->isMonocolored());
		$this->_object->setMonocolored(true);
		$this->assertTrue($this->_object->isMonocolored());
	}
	
	public function testGetMulticolored() : void
	{
		$this->assertNull($this->_object->isMulticolored());
		$this->_object->setMulticolored(true);
		$this->assertTrue($this->_object->isMulticolored());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiParsedMana();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
