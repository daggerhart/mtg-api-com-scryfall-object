<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpMtg\Scryfall\ScryfallApiPreview;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiPreviewTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiPreview
 *
 * @internal
 *
 * @small
 */
class ScryfallApiPreviewTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiPreview
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL PREVIEW', $this->_object->__toString());
	}
	
	public function testGetPreviewedAt() : void
	{
		$this->assertNull($this->_object->getPreviewedAt());
		$datetime = new DateTimeImmutable();
		$this->_object->setPreviewedAt($datetime);
		$this->assertEquals($datetime, $this->_object->getPreviewedAt());
	}
	
	public function testGetSourceUri() : void
	{
		$this->assertNull($this->_object->getSourceUri());
		$uri = new Uri();
		$this->_object->setSourceUri($uri);
		$this->assertEquals($uri, $this->_object->getSourceUri());
	}
	
	public function testGetSource() : void
	{
		$this->assertNull($this->_object->getSource());
		$this->_object->setSource('source');
		$this->assertEquals('source', $this->_object->getSource());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiPreview();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
