<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Uuid\Uuid;
use PhpMtg\Scryfall\ScryfallApiExtension;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiExtensionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiExtension
 *
 * @internal
 *
 * @small
 */
class ScryfallApiExtensionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiExtension
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL EXTENSION 12345678-1234-5678-1234-1234567890ab', $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab), $this->_object->getId());
	}
	
	public function testGetCode() : void
	{
		$this->assertNull($this->_object->getCode());
		$this->_object->setCode('code');
		$this->assertEquals('code', $this->_object->getCode());
	}
	
	public function testGetMtgoCode() : void
	{
		$this->assertNull($this->_object->getMtgoCode());
		$this->_object->setMtgoCode('code');
		$this->assertEquals('code', $this->_object->getMtgoCode());
	}
	
	public function testGetArenaCode() : void
	{
		$this->assertNull($this->_object->getArenaCode());
		$this->_object->setArenaCode('code');
		$this->assertEquals('code', $this->_object->getArenaCode());
	}
	
	public function testGetTcgplayerId() : void
	{
		$this->assertNull($this->_object->getTcgplayerId());
		$this->_object->setTcgplayerId(2);
		$this->assertEquals(2, $this->_object->getTcgplayerId());
	}
	
	public function testGetName() : void
	{
		$this->assertNull($this->_object->getName());
		$this->_object->setName('name');
		$this->assertEquals('name', $this->_object->getName());
	}
	
	public function testGetSetType() : void
	{
		$this->assertNull($this->_object->getSetType());
		$this->_object->setSetType('type');
		$this->assertEquals('type', $this->_object->getSetType());
	}
	
	public function testGetReleasedAt() : void
	{
		$this->assertNull($this->_object->getReleasedAt());
		$datetime = new DateTimeImmutable();
		$this->_object->setReleasedAt($datetime);
		$this->assertEquals($datetime, $this->_object->getReleasedAt());
	}
	
	public function testGetBlockCode() : void
	{
		$this->assertNull($this->_object->getBlockCode());
		$this->_object->setBlockCode('code');
		$this->assertEquals('code', $this->_object->getBlockCode());
	}
	
	public function testGetBlock() : void
	{
		$this->assertNull($this->_object->getBlock());
		$this->_object->setBlock('block');
		$this->assertEquals('block', $this->_object->getBlock());
	}
	
	public function testGetParentSetCode() : void
	{
		$this->assertNull($this->_object->getParentSetCode());
		$this->_object->setParentSetCode('code');
		$this->assertEquals('code', $this->_object->getParentSetCode());
	}
	
	public function testGetCardCount() : void
	{
		$this->assertNull($this->_object->getCardCount());
		$this->_object->setCardCount(2);
		$this->assertEquals(2, $this->_object->getCardCount());
	}
	
	public function testGetDigital() : void
	{
		$this->assertNull($this->_object->isDigital());
		$this->_object->setDigital(true);
		$this->assertTrue($this->_object->isDigital());
	}
	
	public function testGetFoilOnly() : void
	{
		$this->assertNull($this->_object->isFoilOnly());
		$this->_object->setFoilOnly(true);
		$this->assertTrue($this->_object->isFoilOnly());
	}
	
	public function testGetNonFoilOnly() : void
	{
		$this->assertNull($this->_object->isNonfoilOnly());
		$this->_object->setNonfoilOnly(true);
		$this->assertTrue($this->_object->isNonfoilOnly());
	}
	
	public function testGetScryfallUri() : void
	{
		$this->assertNull($this->_object->getScryfallUri());
		$uri = new Uri();
		$this->_object->setScryfallUri($uri);
		$this->assertEquals($uri, $this->_object->getScryfallUri());
	}
	
	public function testGetUri() : void
	{
		$this->assertNull($this->_object->getUri());
		$uri = new Uri();
		$this->_object->setUri($uri);
		$this->assertEquals($uri, $this->_object->getUri());
	}
	
	public function testGetIconSvgUri() : void
	{
		$this->assertNull($this->_object->getIconSvgUri());
		$uri = new Uri();
		$this->_object->setIconSvgUri($uri);
		$this->assertEquals($uri, $this->_object->getIconSvgUri());
	}
	
	public function testGetSearchUri() : void
	{
		$this->assertNull($this->_object->getSearchUri());
		$uri = new Uri();
		$this->_object->setSearchUri($uri);
		$this->assertEquals($uri, $this->_object->getSearchUri());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiExtension(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
