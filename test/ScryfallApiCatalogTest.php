<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpMtg\Scryfall\ScryfallApiCatalog;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiCatalogTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiCatalog
 *
 * @internal
 *
 * @small
 */
class ScryfallApiCatalogTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiCatalog
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL CATALOG', $this->_object->__toString());
	}
	
	public function testGetUri() : void
	{
		$this->assertNull($this->_object->getUri());
		$uri = new Uri();
		$this->_object->setUri($uri);
		$this->assertEquals($uri, $this->_object->getUri());
	}
	
	public function testGetTotalValues() : void
	{
		$this->assertNull($this->_object->getTotalValues());
		$this->_object->setTotalValues(2);
		$this->assertEquals(2, $this->_object->getTotalValues());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals([], $this->_object->getData());
		$this->_object->setData(['datum']);
		$this->assertEquals(['datum'], $this->_object->getData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiCatalog();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
