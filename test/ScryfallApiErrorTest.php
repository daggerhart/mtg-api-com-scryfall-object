<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpMtg\Scryfall\ScryfallApiError;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiErrorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiError
 *
 * @internal
 *
 * @small
 */
class ScryfallApiErrorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiError
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL ERROR', $this->_object->__toString());
	}
	
	public function testGetDetails() : void
	{
		$this->assertNull($this->_object->getDetails());
		$this->_object->setDetails('details');
		$this->assertEquals('details', $this->_object->getDetails());
	}
	
	public function testGetStatus() : void
	{
		$this->assertNull($this->_object->getStatus());
		$this->_object->setStatus(2);
		$this->assertEquals(2, $this->_object->getStatus());
	}
	
	public function testGetType() : void
	{
		$this->assertNull($this->_object->getType());
		$this->_object->setType('type');
		$this->assertEquals('type', $this->_object->getType());
	}
	
	public function testGetWarnings() : void
	{
		$this->assertEquals([], $this->_object->getWarnings());
		$this->_object->setWarnings(['warning']);
		$this->assertEquals(['warning'], $this->_object->getWarnings());
	}
	
	public function testGetErrorType() : void
	{
		$this->assertNull($this->_object->getErrorType());
		$this->_object->setType('type');
		$this->assertEquals('type', $this->_object->getErrorType());
	}
	
	public function testGetErrorCode() : void
	{
		$this->assertNull($this->_object->getErrorCode());
		$this->_object->setStatus(2);
		$this->assertEquals(2, $this->_object->getErrorCode());
	}
	
	public function testGetErrorMessage() : void
	{
		$this->assertNull($this->_object->getErrorMessage());
		$this->_object->setDetails('details');
		$this->assertEquals('details', $this->_object->getErrorMessage());
	}
	
	public function testGetErrorAdditionalInformation() : void
	{
		$this->assertEquals([], $this->_object->getErrorAdditionalInformations());
		$this->_object->setWarnings(['warning']);
		$this->assertEquals(['warning'], $this->_object->getErrorAdditionalInformations());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiError();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
