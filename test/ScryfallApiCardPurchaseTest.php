<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\HttpMessage\Uri;
use PhpMtg\Scryfall\ScryfallApiCardPurchase;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiCardPurchaseTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiCardPurchase
 *
 * @internal
 *
 * @small
 */
class ScryfallApiCardPurchaseTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiCardPurchase
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL CARD PURCHASE', $this->_object->__toString());
	}
	
	public function testGetTcgPlayer() : void
	{
		$this->assertNull($this->_object->getTcgplayer());
		$uri = new Uri(null, null, new Domain('tcgplayer.com'));
		$this->_object->setTcgplayer($uri);
		$this->assertEquals($uri, $this->_object->getTcgplayer());
	}
	
	public function testGetCardmarket() : void
	{
		$this->assertNull($this->_object->getCardmarket());
		$uri = new Uri(null, null, new Domain('cardmarket.eu'));
		$this->_object->setCardmarket($uri);
		$this->assertEquals($uri, $this->_object->getCardmarket());
	}
	
	public function testGetCardhoarder() : void
	{
		$this->assertNull($this->_object->getCardhoarder());
		$uri = new Uri(null, null, new Domain('cardhoarder.com'));
		$this->_object->setCardhoarder($uri);
		$this->assertEquals($uri, $this->_object->getCardhoarder());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiCardPurchase();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
