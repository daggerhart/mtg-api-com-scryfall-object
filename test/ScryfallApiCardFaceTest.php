<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Uuid\Uuid;
use PhpMtg\Scryfall\ScryfallApiCardFace;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiCardFaceTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiCardFace
 *
 * @internal
 *
 * @small
 */
class ScryfallApiCardFaceTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiCardFace
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL CARD FACE', $this->_object->__toString());
	}
	
	public function testGetArtistId() : void
	{
		$this->assertNull($this->_object->getArtistId());
		$this->_object->setArtistId('artistid');
		$this->assertEquals('artistid', $this->_object->getArtistId());
	}
	
	public function testGetArtist() : void
	{
		$this->assertNull($this->_object->getArtist());
		$this->_object->setArtist('artist');
		$this->assertEquals('artist', $this->_object->getArtist());
	}
	
	public function testGetColorIndicator() : void
	{
		$this->assertEquals([], $this->_object->getColorIndicator());
		$this->_object->setColorIndicator(['B']);
		$this->assertEquals(['B'], $this->_object->getColorIndicator());
	}
	
	public function testGetColors() : void
	{
		$this->assertEquals([], $this->_object->getColors());
		$this->_object->setColors(['W']);
		$this->assertEquals(['W'], $this->_object->getColors());
	}
	
	public function testGetFlavorName() : void
	{
		$this->assertNull($this->_object->getFlavorName());
		$this->_object->setFlavorName('flavorname');
		$this->assertEquals('flavorname', $this->_object->getFlavorName());
	}
	
	public function testGetFlavorText() : void
	{
		$this->assertNull($this->_object->getFlavorText());
		$this->_object->setFlavorText('flavortext');
		$this->assertEquals('flavortext', $this->_object->getFlavorText());
	}
	
	public function testGetIllustrationId() : void
	{
		$this->assertNull($this->_object->getIllustrationId());
		$uuid = new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab);
		$this->_object->setIllustrationId($uuid);
		$this->assertEquals($uuid, $this->_object->getIllustrationId());
	}
	
	public function testGetImageUris() : void
	{
		$this->assertEquals([], $this->_object->getImageUris());
		$uri = new Uri();
		$this->_object->setImageUris([$uri]);
		$this->assertEquals([$uri], $this->_object->getImageUris());
	}
	
	public function testGetLoyalty() : void
	{
		$this->assertNull($this->_object->getLoyalty());
		$this->_object->setLoyalty('loyalty');
		$this->assertEquals('loyalty', $this->_object->getLoyalty());
	}
	
	public function testGetManaCost() : void
	{
		$this->assertNull($this->_object->getManaCost());
		$this->_object->setManaCost('manaCost');
		$this->assertEquals('manaCost', $this->_object->getManaCost());
	}
	
	public function testGetName() : void
	{
		$this->assertNull($this->_object->getName());
		$this->_object->setName('name');
		$this->assertEquals('name', $this->_object->getName());
	}
	
	public function testGetOracleText() : void
	{
		$this->assertNull($this->_object->getOracleText());
		$this->_object->setOracleText('oracleText');
		$this->assertEquals('oracleText', $this->_object->getOracleText());
	}
	
	public function testGetPower() : void
	{
		$this->assertNull($this->_object->getPower());
		$this->_object->setPower('power');
		$this->assertEquals('power', $this->_object->getPower());
	}
	
	public function testGetPrintedName() : void
	{
		$this->assertNull($this->_object->getPrintedName());
		$this->_object->setPrintedName('printedName');
		$this->assertEquals('printedName', $this->_object->getPrintedName());
	}
	
	public function testGetPrintedText() : void
	{
		$this->assertNull($this->_object->getPrintedText());
		$this->_object->setPrintedText('printedText');
		$this->assertEquals('printedText', $this->_object->getPrintedText());
	}
	
	public function testGetPrintedTypeline() : void
	{
		$this->assertNull($this->_object->getPrintedTypeLine());
		$this->_object->setPrintedTypeLine('printedTypeLine');
		$this->assertEquals('printedTypeLine', $this->_object->getPrintedTypeLine());
	}
	
	public function testGetToughness() : void
	{
		$this->assertNull($this->_object->getToughness());
		$this->_object->setToughness('toughness');
		$this->assertEquals('toughness', $this->_object->getToughness());
	}
	
	public function testGetTypeline() : void
	{
		$this->assertNull($this->_object->getTypeLine());
		$this->_object->setTypeLine('typeLine');
		$this->assertEquals('typeLine', $this->_object->getTypeLine());
	}
	
	public function testGetWatermark() : void
	{
		$this->assertNull($this->_object->getWatermark());
		$this->_object->setWatermark('watermark');
		$this->assertEquals('watermark', $this->_object->getWatermark());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiCardFace();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
