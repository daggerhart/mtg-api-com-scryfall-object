<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpExtended\Uuid\Uuid;
use PhpMtg\Scryfall\ScryfallApiBulk;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiBulkTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiBulk
 *
 * @internal
 *
 * @small
 */
class ScryfallApiBulkTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiBulk
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL BULK DATA 12345678-1234-5678-1234-1234567890ab', $this->_object->__toString());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab), $this->_object->getId());
	}
	
	public function testGetName() : void
	{
		$this->assertNull($this->_object->getName());
		$this->_object->setName('name');
		$this->assertEquals('name', $this->_object->getName());
	}
	
	public function testGetType() : void
	{
		$this->assertNull($this->_object->getType());
		$this->_object->setType('type');
		$this->assertEquals('type', $this->_object->getType());
	}
	
	public function testGetDescription() : void
	{
		$this->assertNull($this->_object->getDescription());
		$this->_object->setDescription('description');
		$this->assertEquals('description', $this->_object->getDescription());
	}
	
	public function testGetPermalink() : void
	{
		$this->assertNull($this->_object->getPermalinkUri());
		$uri = new Uri();
		$this->_object->setPermalinkUri($uri);
		$this->assertEquals($uri, $this->_object->getPermalinkUri());
	}
	
	public function testGetUpdatedAt() : void
	{
		$this->assertNull($this->_object->getUpdatedAt());
		$datetime = new DateTimeImmutable();
		$this->_object->setUpdatedAt($datetime);
		$this->assertEquals($datetime, $this->_object->getUpdatedAt());
	}
	
	public function testGetSize() : void
	{
		$this->assertNull($this->_object->getSize());
		$this->_object->setSize(123);
		$this->assertEquals(123, $this->_object->getSize());
	}
	
	public function testGetContentType() : void
	{
		$this->assertNull($this->_object->getContentType());
		$this->_object->setContentType('contentType');
		$this->assertEquals('contentType', $this->_object->getContentType());
	}
	
	public function testGetContentEncoding() : void
	{
		$this->assertNull($this->_object->getContentEncoding());
		$this->_object->setContentEncoding('contentEncoding');
		$this->assertEquals('contentEncoding', $this->_object->getContentEncoding());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiBulk(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
