<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\HttpMessage\Uri;
use PhpMtg\Scryfall\ScryfallApiCardResource;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiCardResourceTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiCardResource
 *
 * @internal
 *
 * @small
 */
class ScryfallApiCardResourceTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiCardResource
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL CARD RESOURCE', $this->_object->__toString());
	}
	
	public function testGetGatherer() : void
	{
		$this->assertNull($this->_object->getGatherer());
		$uri = new Uri(null, null, new Domain('gatherer.wizards.com'));
		$this->_object->setGatherer($uri);
		$this->assertEquals($uri, $this->_object->getGatherer());
	}
	
	public function testGetTcgplayer() : void
	{
		$this->assertNull($this->_object->getTcgplayerDecks());
		$uri = new Uri(null, null, new Domain('tcgplayer.com'));
		$this->_object->setTcgplayerDecks($uri);
		$this->assertEquals($uri, $this->_object->getTcgplayerDecks());
	}
	
	public function testGetTcgplayerInfiniteArticles() : void
	{
		$this->assertNull($this->_object->getTcgplayerInfiniteArticles());
		$uri = new Uri(null, null, new Domain('articles.tcgplayer.com'));
		$this->_object->setTcgplayerInfiniteArticles($uri);
		$this->assertEquals($uri, $this->_object->getTcgplayerInfiniteArticles());
	}
	
	public function testGetTcgplayerInfiniteDecks() : void
	{
		$this->assertNull($this->_object->getTcgplayerInfiniteDecks());
		$uri = new Uri(null, null, new Domain('decks.tcgplayer.com'));
		$this->_object->setTcgplayerInfiniteDecks($uri);
		$this->assertEquals($uri, $this->_object->getTcgplayerInfiniteDecks());
	}
	
	public function testGetEdhrec() : void
	{
		$this->assertNull($this->_object->getEdhrec());
		$uri = new Uri(null, null, new Domain('edhrec.org'));
		$this->_object->setEdhrec($uri);
		$this->assertEquals($uri, $this->_object->getEdhrec());
	}
	
	public function testGetMtgtop8() : void
	{
		$this->assertNull($this->_object->getMtgtop8());
		$uri = new Uri(null, null, new Domain('mtgtop8.com'));
		$this->_object->setMtgtop8($uri);
		$this->assertEquals($uri, $this->_object->getMtgtop8());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiCardResource();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
