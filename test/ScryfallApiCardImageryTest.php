<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\HttpMessage\Uri;
use PhpMtg\Scryfall\ScryfallApiCardImagery;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiCardImageryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiCardImagery
 *
 * @internal
 *
 * @small
 */
class ScryfallApiCardImageryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiCardImagery
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL CARD IMAGERY', $this->_object->__toString());
	}
	
	public function testGetSmall() : void
	{
		$this->assertNull($this->_object->getSmall());
		$uri = new Uri(null, null, new Domain('small.org'));
		$this->_object->setSmall($uri);
		$this->assertEquals($uri, $this->_object->getSmall());
	}
	
	public function testGetNormal() : void
	{
		$this->assertNull($this->_object->getNormal());
		$uri = new Uri(null, null, new Domain('normal.org'));
		$this->_object->setNormal($uri);
		$this->assertEquals($uri, $this->_object->getNormal());
	}
	
	public function testGetLarge() : void
	{
		$this->assertNull($this->_object->getLarge());
		$uri = new Uri(null, null, new Domain('large.org'));
		$this->_object->setLarge($uri);
		$this->assertEquals($uri, $this->_object->getLarge());
	}
	
	public function testGetPng() : void
	{
		$this->assertNull($this->_object->getPng());
		$uri = new Uri(null, null, new Domain('png.org'));
		$this->_object->setPng($uri);
		$this->assertEquals($uri, $this->_object->getPng());
	}
	
	public function testGetArtCrop() : void
	{
		$this->assertNull($this->_object->getArtCrop());
		$uri = new Uri(null, null, new Domain('art.org'));
		$this->_object->setArtCrop($uri);
		$this->assertEquals($uri, $this->_object->getArtCrop());
	}
	
	public function testGetBorderCrop() : void
	{
		$this->assertNull($this->_object->getBorderCrop());
		$uri = new Uri(null, null, new Domain('border.org'));
		$this->_object->setBorderCrop($uri);
		$this->assertEquals($uri, $this->_object->getBorderCrop());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiCardImagery();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
