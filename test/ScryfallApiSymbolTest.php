<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\Uri;
use PhpMtg\Scryfall\ScryfallApiSymbol;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiSymbolTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiSymbol
 *
 * @internal
 *
 * @small
 */
class ScryfallApiSymbolTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiSymbol
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL SYMBOL symbol', $this->_object->__toString());
	}
	
	public function testGetSymbol() : void
	{
		$this->assertEquals('symbol', $this->_object->getSymbol());
	}
	
	public function testGetLooseVariant() : void
	{
		$this->assertNull($this->_object->getLooseVariant());
		$this->_object->setLooseVariant('variant');
		$this->assertEquals('variant', $this->_object->getLooseVariant());
	}
	
	public function testGetEnglish() : void
	{
		$this->assertNull($this->_object->getEnglish());
		$this->_object->setEnglish('english');
		$this->assertEquals('english', $this->_object->getEnglish());
	}
	
	public function testGetTransposable() : void
	{
		$this->assertNull($this->_object->isTransposable());
		$this->_object->setTransposable(true);
		$this->assertTrue($this->_object->isTransposable());
	}
	
	public function testGetRepresentsMana() : void
	{
		$this->assertNull($this->_object->isRepresentsMana());
		$this->_object->setRepresentsMana(true);
		$this->assertTrue($this->_object->isRepresentsMana());
	}
	
	public function testGetCmc() : void
	{
		$this->assertNull($this->_object->getCmc());
		$this->_object->setCmc(2);
		$this->assertEquals(2, $this->_object->getCmc());
	}
	
	public function testGetAppearsInManaCosts() : void
	{
		$this->assertNull($this->_object->isAppearsInManaCosts());
		$this->_object->setAppearsInManaCosts(true);
		$this->assertTrue($this->_object->isAppearsInManaCosts());
	}
	
	public function testGetFunny() : void
	{
		$this->assertNull($this->_object->isFunny());
		$this->_object->setFunny(true);
		$this->assertTrue($this->_object->isFunny());
	}
	
	public function testGetColors() : void
	{
		$this->assertEquals([], $this->_object->getColors());
		$this->_object->setColors(['B']);
		$this->assertEquals(['B'], $this->_object->getColors());
	}
	
	public function testGetGathererAlternatives() : void
	{
		$this->assertEquals([], $this->_object->getGathererAlternates());
		$this->_object->setGathererAlternates(['oB']);
		$this->assertEquals(['oB'], $this->_object->getGathererAlternates());
	}
	
	public function testGetSvgUri() : void
	{
		$this->assertNull($this->_object->getSvgUri());
		$uri = new Uri();
		$this->_object->setSvgUri($uri);
		$this->assertEquals($uri, $this->_object->getSvgUri());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiSymbol('symbol');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
