<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpMtg\Scryfall\ScryfallApiPagination;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiPaginationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiPagination
 *
 * @internal
 *
 * @small
 */
class ScryfallApiPaginationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiPagination
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL PAGINATION', $this->_object->__toString());
	}
	
	public function testGetHasMore() : void
	{
		$this->assertNull($this->_object->hasMore());
		$this->_object->setHasMore(true);
		$this->assertTrue($this->_object->hasMore());
	}
	
	public function testGetNextPage() : void
	{
		$this->assertNull($this->_object->getNextPage());
		$this->_object->setNextPage(2);
		$this->assertEquals(2, $this->_object->getNextPage());
	}
	
	public function testGetTotalCards() : void
	{
		$this->assertNull($this->_object->getTotalCards());
		$this->_object->setTotalCards(2);
		$this->assertEquals(2, $this->_object->getTotalCards());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals(new ArrayIterator(), $this->_object->getData());
		$this->_object->setData(new ArrayIterator(['WB']));
		$this->assertEquals(new ArrayIterator(['WB']), $this->_object->getData());
	}
	
	public function testGetIterator() : void
	{
		$this->assertEquals(new ArrayIterator(), $this->_object->getIterator());
		$this->_object->setData(new ArrayIterator(['WB']));
		$this->assertEquals(new ArrayIterator(['WB']), $this->_object->getIterator());
	}
	
	public function testCount() : void
	{
		$this->assertEquals(0, $this->_object->count());
		$this->_object->setData(new ArrayIterator(['WB']));
		$this->assertEquals(1, $this->_object->count());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiPagination();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
