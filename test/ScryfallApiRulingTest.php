<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Uuid\Uuid;
use PhpMtg\Scryfall\ScryfallApiRuling;
use PHPUnit\Framework\TestCase;

/**
 * ScryfallApiRulingTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Scryfall\ScryfallApiRuling
 *
 * @internal
 *
 * @small
 */
class ScryfallApiRulingTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ScryfallApiRuling
	 */
	protected $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('SCRYFALL RULING 12345678-1234-5678-1234-1234567890ab', $this->_object->__toString());
	}
	
	public function testGetOracleId() : void
	{
		$this->assertEquals(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab), $this->_object->getOracleId());
	}
	
	public function testGetSource() : void
	{
		$this->assertNull($this->_object->getSource());
		$this->_object->setSource('source');
		$this->assertEquals('source', $this->_object->getSource());
	}
	
	public function testGetPublishedAt() : void
	{
		$this->assertNull($this->_object->getPublishedAt());
		$datetime = new DateTimeImmutable();
		$this->_object->setPublishedAt($datetime);
		$this->assertEquals($datetime, $this->_object->getPublishedAt());
	}
	
	public function testGetComment() : void
	{
		$this->assertNull($this->_object->getComment());
		$this->_object->setComment('comment');
		$this->assertEquals('comment', $this->_object->getComment());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ScryfallApiRuling(new Uuid(0x12345678, 0x1234, 0x5678, 0x12, 0x34, 0x123456, 0x7890ab));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		$this->_object = null;
	}
	
}
