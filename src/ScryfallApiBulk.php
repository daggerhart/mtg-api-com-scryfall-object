<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiBulk class file.
 * 
 * This class represents the bulk data metadata.
 * 
 * @author Anastaszor
 */
class ScryfallApiBulk implements ScryfallApiBulkInterface
{
	
	/**
	 * A unique ID for this bulk item.
	 * 
	 * @var UuidInterface
	 */
	protected $_id;
	
	/**
	 * A human-readable name for this file.
	 * 
	 * @var ?string
	 */
	protected $_name;
	
	/**
	 * The type of bulk data.
	 * 
	 * @var ?string
	 */
	protected $_type;
	
	/**
	 * A human-readable description for this file.
	 * 
	 * @var ?string
	 */
	protected $_description;
	
	/**
	 * The URL that hosts this bulk file.
	 * 
	 * @var ?UriInterface
	 */
	protected $_permalinkUri;
	
	/**
	 * The time when this file was last updated.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected $_updatedAt;
	
	/**
	 * The size of this file in integer bytes.
	 * 
	 * @var ?integer
	 */
	protected $_size;
	
	/**
	 * The MIME type of this file.
	 * 
	 * @var ?string
	 */
	protected $_contentType;
	
	/**
	 * The Content-Encoding encoding that will be used to transmit this file
	 * when you download it.
	 * 
	 * @var ?string
	 */
	protected $_contentEncoding;
	
	/**
	 * Builds a new ScryfallApiBulk with its data.
	 * 
	 * @param UuidInterface $id
	 */
	public function __construct(UuidInterface $id)
	{
		$this->_id = $id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL BULK DATA '.$this->_id->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiBulkInterface::getId()
	 */
	public function getId() : UuidInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the name of this file.
	 * 
	 * @param ?string $name
	 * @return ScryfallApiBulk
	 */
	public function setName(?string $name) : ScryfallApiBulk
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiBulkInterface::getName()
	 */
	public function getName() : ?string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the type of this file.
	 * 
	 * @param ?string $type
	 * @return ScryfallApiBulk
	 */
	public function setType(?string $type) : ScryfallApiBulk
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiBulkInterface::getType()
	 */
	public function getType() : ?string
	{
		return $this->_type;
	}
	
	/**
	 * Sets the description of this file.
	 * 
	 * @param ?string $description
	 * @return ScryfallApiBulk
	 */
	public function setDescription(?string $description) : ScryfallApiBulk
	{
		$this->_description = $description;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiBulkInterface::getDescription()
	 */
	public function getDescription() : ?string
	{
		return $this->_description;
	}
	
	/**
	 * Sets the permalink uri of this file.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiBulk
	 */
	public function setPermalinkUri(?UriInterface $uri) : ScryfallApiBulk
	{
		$this->_permalinkUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiBulkInterface::getPermalinkUri()
	 */
	public function getPermalinkUri() : ?UriInterface
	{
		return $this->_permalinkUri;
	}
	
	/**
	 * Sets the updated datetime.
	 * 
	 * @param ?DateTimeInterface $datetime
	 * @return ScryfallApiBulk
	 */
	public function setUpdatedAt(?DateTimeInterface $datetime) : ScryfallApiBulk
	{
		$this->_updatedAt = $datetime;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiBulkInterface::getUpdatedAt()
	 */
	public function getUpdatedAt() : ?DateTimeInterface
	{
		return $this->_updatedAt;
	}
	
	/**
	 * Sets the size of this file.
	 * 
	 * @param ?integer $size
	 * @return ScryfallApiBulk
	 */
	public function setSize(?int $size) : ScryfallApiBulk
	{
		$this->_size = $size;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiBulkInterface::getSize()
	 */
	public function getSize() : ?int
	{
		return $this->_size;
	}
	
	/**
	 * Sets the content type of this file.
	 * 
	 * @param ?string $contentType
	 * @return ScryfallApiBulk
	 */
	public function setContentType(?string $contentType) : ScryfallApiBulk
	{
		$this->_contentType = $contentType;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiBulkInterface::getContentType()
	 */
	public function getContentType() : ?string
	{
		return $this->_contentType;
	}
	
	/**
	 * Sets the content encoding of this file.
	 * 
	 * @param ?string $encoding
	 * @return ScryfallApiBulk
	 */
	public function setContentEncoding(?string $encoding) : ScryfallApiBulk
	{
		$this->_contentEncoding = $encoding;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiBulkInterface::getContentEncoding()
	 */
	public function getContentEncoding() : ?string
	{
		return $this->_contentEncoding;
	}
	
}
