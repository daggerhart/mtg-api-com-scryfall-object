<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use ArrayIterator;
use DateTimeInterface;
use Iterator;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiCard class file.
 *
 * This class represents a card from the scryfall api.
 *
 * @author Anastaszor
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ScryfallApiCard implements ScryfallApiCardInterface
{
	
	// ---- ---- core card fields ---- ---- \\
	
	/**
	 * A unique ID for this card in Scryfall’s database.
	 * 
	 * @var UuidInterface
	 */
	protected $_id;
	
	/**
	 * This card’s Arena ID, if any. A large percentage of cards are not
	 * available on Arena and do not have this ID. 
	 * 
	 * @var ?integer
	 */
	protected $_arenaId;
	
	/**
	 * A language code for this printing.
	 * 
	 * @var ?string
	 */
	protected $_lang;
	
	/**
	 * This card’s Magic Online ID (also known as the Catalog ID), if any.
	 * A large percentage of cards are not available on Magic Online and do
	 * not have this ID. 
	 * 
	 * @var ?integer
	 */
	protected $_mtgoId;
	
	/**
	 * This card’s foil Magic Online ID (also known as the Catalog ID), if any.
	 * A large percentage of cards are not available on Magic Online and do
	 * not have this ID. 
	 * 
	 * @var ?integer
	 */
	protected $_mtgoFoilId;
	
	/**
	 * This card’s multiverse IDs on Gatherer, if any, as an array of integers.
	 * Note that Scryfall includes many promo cards, tokens, and other esoteric
	 * objects that do not have these identifiers.
	 * 
	 * @var array<integer, integer>
	 */
	protected $_multiverseIds = [];
	
	/**
	 * This card’s ID on TCGplayer’s API, also known as the productId.
	 * 
	 * @var ?integer
	 */
	protected $_tcgplayerId;
	
	/**
	 * This card's ID on TCGplayer's API for the etched version.
	 * 
	 * @var ?integer
	 */
	protected $_tcgplayerEtchedId;
	
	/**
	 * This card’s ID on Cardmarket’s API, also known as the idProduct.
	 * 
	 * @var ?integer
	 */
	protected $_cardmarketId;
	
	/**
	 * A unique ID for this card’s oracle identity. This value is consistent
	 * across reprinted card editions, and unique among different cards with
	 * the same name (tokens, Unstable variants, etc).
	 * 
	 * @var ?UuidInterface
	 */
	protected $_oracleId;
	
	/**
	 * A link to where you can begin paginating all re/prints for this card
	 * on Scryfall’s API. 
	 * 
	 * @var ?UriInterface
	 */
	protected $_printsSearchUri;
	
	/**
	 * A link to this card’s rulings list on Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	protected $_rulingsUri;
	
	/**
	 * A link to this card’s permapage on Scryfall’s website.
	 * 
	 * @var ?UriInterface
	 */
	protected $_scryfallUri;
	
	/**
	 * A link to this card object on Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	protected $_uri;
	
	// ---- ---- gameplay fields ---- ---- \\
	
	/**
	 * If this card is closely related to other cards, this property will be
	 * an array with Related Card Objects.
	 * 
	 * @var ?Iterator<integer, ScryfallApiRelatedCardInterface>
	 */
	protected $_allParts;
	
	/**
	 * An array of Card Face objects, if this card is multifaced.
	 * 
	 * @var ?Iterator<integer, ScryfallApiCardFaceInterface>
	 */
	protected $_cardFaces;
	
	/**
	 * The card’s converted mana cost. Note that some funny cards have
	 * fractional mana costs. 
	 * 
	 * @var ?float
	 */
	protected $_cmc;
	
	/**
	 * This card’s colors, if the overall card has colors defined by the rules.
	 * Otherwise the colors will be on the card_faces objects, see below.
	 * 
	 * @var array<integer, string>
	 */
	protected $_colors = [];
	
	/**
	 * This card’s color identity.
	 * 
	 * @var array<integer, string>
	 */
	protected $_colorIdentity = [];
	
	/**
	 * The colors in this card’s color indicator, if any. A null value for
	 * this field indicates the card does not have one.
	 * 
	 * @var array<integer, string>
	 */
	protected $_colorIndicator = [];
	
	/**
	 * This card’s overall rank/popularity on EDHREC. Not all cards are ranked.
	 * 
	 * @var ?integer
	 */
	protected $_edhrecRank;
	
	/**
	 * True if this printing exists in a foil version.
	 * 
	 * @var ?boolean
	 */
	protected $_foil;
	
	/**
	 * The finishes of this card.
	 * 
	 * @var array<integer, string>
	 */
	protected $_finishes = [];
	
	/**
	 * This card’s hand modifier, if it is Vanguard card. This value will
	 * contain a delta, such as -1.
	 * 
	 * @var ?string
	 */
	protected $_handModifier;
	
	/**
	 * An array of keywords that this card uses, such as 'Flying' and
	 * 'Cumulative upkeep'.
	 * 
	 * @var array<integer, string>
	 */
	protected $_keywords = [];
	
	/**
	 * The produced mana, if this card produces any.
	 * 
	 * @var array<integer, string>
	 */
	protected $_producedMana = [];
	
	/**
	 * A code for this card’s layout.
	 * 
	 * @var ?string
	 */
	protected $_layout;
	
	/**
	 * An object describing the legality of this card across play formats.
	 * Possible legalities are legal, not_legal, restricted, and banned.
	 * 
	 * @var ?ScryfallApiCardLegalityInterface
	 */
	protected $_legalities;
	
	/**
	 * This card’s life modifier, if it is Vanguard card. This value will
	 * contain a delta, such as +2.
	 * 
	 * @var ?string
	 */
	protected $_lifeModifier;
	
	/**
	 * This loyalty if any. Note that some cards have loyalties that are not
	 * numeric, such as X.
	 * 
	 * @var ?string
	 */
	protected $_loyalty;
	
	/**
	 * The mana cost for this card. This value will be any empty string "" if
	 * the cost is absent. Remember that per the game rules, a missing mana
	 * cost and a mana cost of {0} are different values. Multi-faced cards
	 * will report this value in card faces.
	 * 
	 * @var ?string
	 */
	protected $_manaCost;
	
	/**
	 * The name of this card. If this card has multiple faces, this field will
	 * contain both names separated by _ // _ .
	 * 
	 * @var ?string
	 */
	protected $_name;
	
	/**
	 * True if this printing exists in a nonfoil version.
	 * 
	 * @var ?boolean
	 */
	protected $_nonfoil;
	
	/**
	 * The Oracle text for this card, if any.
	 * 
	 * @var ?string
	 */
	protected $_oracleText;
	
	/**
	 * True if this card is oversized.
	 * 
	 * @var ?boolean
	 */
	protected $_oversized;
	
	/**
	 * This card’s rank/popularity on Penny Dreadful. Not all cards are ranked.
	 * 
	 * @var ?integer
	 */
	protected $_pennyRank;
	
	/**
	 * This card’s power, if any. Note that some cards have powers that are
	 * not numeric, such as *.
	 * 
	 * @var ?string
	 */
	protected $_power;
	
	/**
	 * True if this card is on the Reserved List.
	 * 
	 * @var ?boolean
	 */
	protected $_reserved;
	
	/**
	 * The security stamp value (normal or acorn).
	 * 
	 * @var ?string
	 */
	protected $_securityStamp;
	
	/**
	 * This card’s toughness, if any. Note that some cards have toughnesses
	 * that are not numeric, such as *.
	 * 
	 * @var ?string
	 */
	protected $_toughness;
	
	/**
	 * The type line of this card.
	 * 
	 * @var ?string
	 */
	protected $_typeLine;
	
	// ---- ---- print fields ---- ---- \\
	
	/**
	 * The id of the artists.
	 * 
	 * @var array<integer, string>
	 */
	protected $_artistIds = [];
	
	/**
	 * The name of the illustrator of this card. Newly spoiled cards may not
	 * have this field yet.
	 * 
	 * @var ?string
	 */
	protected $_artist;
	
	/**
	 * 	Whether this card is found in boosters.
	 * 
	 * @var ?boolean
	 */
	protected $_booster;
	
	/**
	 * This card’s border color: black, borderless, gold, silver, or white.
	 * 
	 * @var ?string
	 */
	protected $_borderColor;
	
	/**
	 * The Scryfall ID for the card back design present on this card.
	 * 
	 * @var ?UuidInterface
	 */
	protected $_cardBackId;
	
	/**
	 * This card’s collector number. Note that collector numbers can contain
	 * non-numeric characters, such as letters or ★.
	 * 
	 * @var ?string
	 */
	protected $_collectorNumber;
	
	/**
	 * True if this is a digital card on Magic Online.
	 * 
	 * @var ?boolean
	 */
	protected $_digital;
	
	/**
	 * The flavor text, if any.
	 * 
	 * @var ?string
	 */
	protected $_flavorText;
	
	/**
	 * This card’s frame effects, if any.
	 * 
	 * legendary			The cards have a legendary crown
	 * miracle				The miracle frame effect
	 * nyxtouched			The Nyx-touched frame effect
	 * draft				The draft-matters frame effect
	 * devoid				The Devoid frame effect
	 * tombstone			The Odyssey tombstone mark
	 * colorshifted			A colorshifted frame
	 * inverted				The FNM-style inverted frame
	 * sunmoondfc			The sun and moon transform marks
	 * compasslanddfc		The compass and land transform marks
	 * originpwdfc			The Origins and planeswalker transform marks
	 * mooneldrazidfc		The moon and Eldrazi transform marks
	 * moonreversemoondfc	The waxing and waning crescent moon transform marks
	 * showcase				A custom Showcase frame
	 * extendedart			An extended art frame
	 * 
	 * @var array<integer, string>
	 */
	protected $_frameEffects = [];
	
	/**
	 * This card’s frame layout.
	 * 
	 * 1993		The original Magic card frame, starting from Limited Edition Alpha.
	 * 1997		The updated classic frame starting from Mirage block
	 * 2003		The “modern” Magic card frame, introduced in Eighth Edition and Mirrodin block.
	 * 2015		The holofoil-stamp Magic card frame, introduced in Magic 2015.
	 * future	The frame used on cards from the future
	 * 
	 * @var ?string
	 */
	protected $_frame;
	
	/**
	 * True if this card’s artwork is larger than normal.
	 * 
	 * @var ?boolean
	 */
	protected $_fullArt;
	
	/**
	 * A list of games that this card print is available in, paper, arena,
	 * and/or mtgo.
	 * 
	 * @var array<integer, string>
	 */
	protected $_games = [];
	
	/**
	 * True if this card’s imagery is high resolution.
	 * 
	 * @var ?boolean
	 */
	protected $_highresImage;
	
	/**
	 * Gets the value of the status of the image (like "highres_scan").
	 * 
	 * @var ?string
	 */
	protected $_imageStatus;
	
	/**
	 * A unique identifier for the card artwork that remains consistent across
	 * reprints. Newly spoiled cards may not have this field yet.
	 * 
	 * @var ?UuidInterface
	 */
	protected $_illustrationId;
	
	/**
	 * An object listing available imagery for this card. See the Card Imagery
	 * article for more information.
	 * 
	 * @var ?ScryfallApiCardImageryInterface
	 */
	protected $_imageUris;
	
	/**
	 * An object containing daily price information for this card, including
	 * usd, usd_foil, eur, and tix prices, as strings.
	 * 
	 * @var ?ScryfallApiPricesInterface
	 */
	protected $_prices;
	
	/**
	 * The localized name printed on this card, if any.
	 * 
	 * @var ?string
	 */
	protected $_printedName;
	
	/**
	 * The localized text printed on this card, if any.
	 * 
	 * @var ?string
	 */
	protected $_printedText;
	
	/**
	 * The localized type line printed on this card, if any.
	 * 
	 * @var ?string
	 */
	protected $_printedTypeLine;
	
	/**
	 * True if this card is a promotional print.
	 * 
	 * @var ?boolean
	 */
	protected $_promo;
	
	/**
	 * An array of strings describing what categories of promo cards this card
	 * falls into.
	 * 
	 * @var array<integer, string>
	 */
	protected $_promoTypes = [];
	
	/**
	 * An object providing URIs to this card’s listing on major marketplaces.
	 * 
	 * @var ?ScryfallApiCardPurchaseInterface
	 */
	protected $_purchaseUris;
	
	/**
	 * This card’s rarity. One of common, uncommon, rare, or mythic.
	 * 
	 * @var ?string
	 */
	protected $_rarity;
	
	/**
	 * An object providing URIs to this card’s listing on other Magic: The
	 * Gathering online resources.
	 * 
	 * @var ?ScryfallApiCardResourceInterface
	 */
	protected $_relatedUris;
	
	/**
	 * The date this card was first released.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected $_releasedAt;
	
	/**
	 * True if this card is a reprint.
	 * 
	 * @var ?boolean
	 */
	protected $_reprint;
	
	/**
	 * A link to this card’s set on Scryfall’s website.
	 * 
	 * @var ?UriInterface
	 */
	protected $_scryfallSetUri;
	
	/**
	 * This card’s set code.
	 * 
	 * @var ?string
	 */
	protected $_setId;
	
	/**
	 * This card's set code.
	 * 
	 * @var ?string
	 */
	protected $_set;
	
	/**
	 * This card’s full set name.
	 * 
	 * @var ?string
	 */
	protected $_setName;
	
	/**
	 * A link to where you can begin paginating this card’s set on the
	 * Scryfall API.
	 * 
	 * @var ?UriInterface
	 */
	protected $_setSearchUri;
	
	/**
	 * 	The type of set this printing is in.
	 * 
	 * @var ?string
	 */
	protected $_setType;
	
	/**
	 * A link to this card’s set object on Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	protected $_setUri;
	
	/**
	 * True if this card is a Story Spotlight.
	 * 
	 * @var ?boolean
	 */
	protected $_storySpotlight;
	
	/**
	 * True if the card is printed without text.
	 * 
	 * @var ?boolean
	 */
	protected $_textless;
	
	/**
	 * 	Whether this card is a variation of another printing.
	 * 
	 * @var ?boolean
	 */
	protected $_variation;
	
	/**
	 * The printing ID of the printing this card is a variation of.
	 * 
	 * @var ?UuidInterface
	 */
	protected $_variationOf;
	
	/**
	 * This card’s watermark, if any.
	 * 
	 * @var ?string
	 */
	protected $_watermark;
	
	/**
	 * The preview data for this card.
	 * 
	 * @var ?ScryfallApiPreviewInterface
	 */
	protected $_preview;
	
	/**
	 * Builds a new ScryfallApiCard with its data.
	 * 
	 * @param UuidInterface $id
	 */
	public function __construct(UuidInterface $id)
	{
		$this->_id = $id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL CARD '.$this->_id->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getId()
	 */
	public function getId() : UuidInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the arena id of this card.
	 * 
	 * @param ?integer $arenaId
	 * @return ScryfallApiCard
	 */
	public function setArenaId(?int $arenaId) : ScryfallApiCard
	{
		$this->_arenaId = $arenaId;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getArenaId()
	 */
	public function getArenaId() : ?int
	{
		return $this->_arenaId;
	}
	
	/**
	 * Sets the lang of this card.
	 * 
	 * @param ?string $lang
	 * @return ScryfallApiCard
	 */
	public function setLang(?string $lang) : ScryfallApiCard
	{
		$this->_lang = $lang;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getLang()
	 */
	public function getLang() : ?string
	{
		return $this->_lang;
	}
	
	/**
	 * Sets the mtgo id of this card.
	 * 
	 * @param ?integer $mtgoId
	 * @return ScryfallApiCard
	 */
	public function setMtgoId(?int $mtgoId) : ScryfallApiCard
	{
		$this->_mtgoId = $mtgoId;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getMtgoId()
	 */
	public function getMtgoId() : ?int
	{
		return $this->_mtgoId;
	}
	
	/**
	 * Sets the mtgo foil id.
	 * 
	 * @param ?integer $mtgoFoilId
	 * @return ScryfallApiCard
	 */
	public function setMtgoFoilId(?int $mtgoFoilId) : ScryfallApiCard
	{
		$this->_mtgoFoilId = $mtgoFoilId;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getMtgoFoilId()
	 */
	public function getMtgoFoilId() : ?int
	{
		return $this->_mtgoFoilId;
	}
	
	/**
	 * Sets the multiverse ids of this card.
	 * 
	 * @param array<integer, integer> $multiverseids
	 * @return ScryfallApiCard
	 */
	public function setMultiverseIds(array $multiverseids) : ScryfallApiCard
	{
		$this->_multiverseIds = $multiverseids;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getMultiverseIds()
	 */
	public function getMultiverseIds() : array
	{
		return $this->_multiverseIds;
	}
	
	/**
	 * Sets the tcgplayer id.
	 * 
	 * @param ?integer $tcgplayerId
	 * @return ScryfallApiCard
	 */
	public function setTcgplayerId(?int $tcgplayerId) : ScryfallApiCard
	{
		$this->_tcgplayerId = $tcgplayerId;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getTcgplayerId()
	 */
	public function getTcgplayerId() : ?int
	{
		return $this->_tcgplayerId;
	}
	
	/**
	 * Sets the tcgplayer etched id.
	 * 
	 * @param ?integer $tcgplayerEtchedId
	 * @return ScryfallApiCard
	 */
	public function setTcgplayerEtchedId(?int $tcgplayerEtchedId) : ScryfallApiCard
	{
		$this->_tcgplayerEtchedId = $tcgplayerEtchedId;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getTcgplayerEtchedId()
	 */
	public function getTcgplayerEtchedId() : ?int
	{
		return $this->_tcgplayerEtchedId;
	}
	
	/**
	 * Sets the cardmarket id of this card.
	 * 
	 * @param ?integer $cardmarketId
	 * @return ScryfallApiCard
	 */
	public function setCardmarketId(?int $cardmarketId) : ScryfallApiCard
	{
		$this->_cardmarketId = $cardmarketId;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getCardmarketId()
	 */
	public function getCardmarketId() : ?int
	{
		return $this->_cardmarketId;
	}
	
	/**
	 * Sets the oracle id of this card.
	 * 
	 * @param ?UuidInterface $uuid
	 * @return ScryfallApiCard
	 */
	public function setOracleId(?UuidInterface $uuid) : ScryfallApiCard
	{
		$this->_oracleId = $uuid;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getOracleId()
	 */
	public function getOracleId() : ?UuidInterface
	{
		return $this->_oracleId;
	}
	
	/**
	 * Sets the prints search uri of this card.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCard
	 */
	public function setPrintsSearchUri(?UriInterface $uri) : ScryfallApiCard
	{
		$this->_printsSearchUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPrintsSearchUri()
	 */
	public function getPrintsSearchUri() : ?UriInterface
	{
		return $this->_printsSearchUri;
	}
	
	/**
	 * Sets the rulings uri of this card.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCard
	 */
	public function setRulingsUri(?UriInterface $uri) : ScryfallApiCard
	{
		$this->_rulingsUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getRulingsUri()
	 */
	public function getRulingsUri() : ?UriInterface
	{
		return $this->_rulingsUri;
	}
	
	/**
	 * Sets the scryfall uri of this card.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCard
	 */
	public function setScryfallUri(?UriInterface $uri) : ScryfallApiCard
	{
		$this->_scryfallUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getScryfallUri()
	 */
	public function getScryfallUri() : ?UriInterface
	{
		return $this->_scryfallUri;
	}
	
	/**
	 * Sets the uri of this card.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCard
	 */
	public function setUri(?UriInterface $uri) : ScryfallApiCard
	{
		$this->_uri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getUri()
	 */
	public function getUri() : ?UriInterface
	{
		return $this->_uri;
	}
	
	/**
	 * Sets all parts of this card.
	 * 
	 * @param Iterator<integer, ScryfallApiRelatedCardInterface> $parts
	 * @return ScryfallApiCard
	 */
	public function setAllParts(Iterator $parts) : ScryfallApiCard
	{
		$this->_allParts = $parts;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getAllParts()
	 */
	public function getAllParts() : Iterator
	{
		return $this->_allParts ?? new ArrayIterator();
	}
	
	/**
	 * Sets the card faces of this card.
	 * 
	 * @param Iterator<integer, ScryfallApiCardFaceInterface> $cardFaces
	 * @return ScryfallApiCard
	 */
	public function setCardFaces(Iterator $cardFaces) : ScryfallApiCard
	{
		$this->_cardFaces = $cardFaces;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getCardFaces()
	 */
	public function getCardFaces() : Iterator
	{
		return $this->_cardFaces ?? new ArrayIterator();
	}
	
	/**
	 * Sets the cmc of this card.
	 * 
	 * @param ?float $cmc
	 * @return ScryfallApiCard
	 */
	public function setCmc(?float $cmc) : ScryfallApiCard
	{
		$this->_cmc = $cmc;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getCmc()
	 */
	public function getCmc() : ?float
	{
		return $this->_cmc;
	}
	
	/**
	 * Sets the colors of this card.
	 * 
	 * @param array<integer, string> $colors
	 * @return ScryfallApiCard
	 */
	public function setColors(array $colors) : ScryfallApiCard
	{
		$this->_colors = $colors;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getColors()
	 */
	public function getColors() : array
	{
		return $this->_colors;
	}
	
	/**
	 * Sets the color identity of this card.
	 * 
	 * @param array<integer, string> $colors
	 * @return ScryfallApiCard
	 */
	public function setColorIdentity(array $colors) : ScryfallApiCard
	{
		$this->_colorIdentity = $colors;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getColorIdentity()
	 */
	public function getColorIdentity() : array
	{
		return $this->_colorIdentity;
	}
	
	/**
	 * Sets the color indicators.
	 * 
	 * @param array<integer, string> $colors
	 * @return ScryfallApiCard
	 */
	public function setColorIndicator(array $colors) : ScryfallApiCard
	{
		$this->_colorIndicator = $colors;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getColorIndicator()
	 */
	public function getColorIndicator() : array
	{
		return $this->_colorIndicator;
	}
	
	/**
	 * Sets the edhrec rank of this card.
	 * 
	 * @param ?integer $edhrec
	 * @return ScryfallApiCard
	 */
	public function setEdhrecRank(?int $edhrec) : ScryfallApiCard
	{
		$this->_edhrecRank = $edhrec;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getEdhrecRank()
	 */
	public function getEdhrecRank() : ?int
	{
		return $this->_edhrecRank;
	}
	
	/**
	 * Sets whether this card is foil.
	 * 
	 * @param ?boolean $isFoil
	 * @return ScryfallApiCard
	 */
	public function setFoil(?bool $isFoil) : ScryfallApiCard
	{
		$this->_foil = $isFoil;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isFoil()
	 */
	public function isFoil() : ?bool
	{
		return $this->_foil;
	}
	
	/**
	 * Sets the finishes of this card.
	 * 
	 * @param array<integer, string> $finishes
	 * @return ScryfallApiCard
	 */
	public function setFinishes(array $finishes) : ScryfallApiCard
	{
		$this->_finishes = $finishes;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getFinishes()
	 */
	public function getFinishes() : array
	{
		return $this->_finishes;
	}
	
	/**
	 * Sets the hand modifier of this card.
	 * 
	 * @param ?string $handModifier
	 * @return ScryfallApiCard
	 */
	public function setHandModifier(?string $handModifier) : ScryfallApiCard
	{
		$this->_handModifier = $handModifier;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getHandModifier()
	 */
	public function getHandModifier() : ?string
	{
		return $this->_handModifier;
	}
	
	/**
	 * Sets the keywords of this card.
	 * 
	 * @param array<integer, string> $keywords
	 * @return ScryfallApiCard
	 */
	public function setKeywords(array $keywords) : ScryfallApiCard
	{
		$this->_keywords = $keywords;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getKeywords()
	 */
	public function getKeywords() : array
	{
		return $this->_keywords;
	}
	
	/**
	 * Sets the manas produced by this card.
	 * 
	 * @param array<integer, string> $manas
	 * @return ScryfallApiCard
	 */
	public function setProducedMana(array $manas) : ScryfallApiCard
	{
		$this->_producedMana = $manas;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getProducedMana()
	 */
	public function getProducedMana() : array
	{
		return $this->_producedMana;
	}
	
	/**
	 * Sets the layout of this card.
	 * 
	 * @param ?string $layout
	 * @return ScryfallApiCard
	 */
	public function setLayout(?string $layout) : ScryfallApiCard
	{
		$this->_layout = $layout;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getLayout()
	 */
	public function getLayout() : ?string
	{
		return $this->_layout;
	}
	
	/**
	 * Sets the legalities of this card.
	 * 
	 * @param ?ScryfallApiCardLegalityInterface $legalities
	 * @return ScryfallApiCard
	 */
	public function setLegalities(?ScryfallApiCardLegalityInterface $legalities) : ScryfallApiCard
	{
		$this->_legalities = $legalities;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getLegalities()
	 */
	public function getLegalities() : ?ScryfallApiCardLegalityInterface
	{
		return $this->_legalities;
	}
	
	/**
	 * Sets the life modifier of this card.
	 * 
	 * @param ?string $lifeModifier
	 * @return ScryfallApiCard
	 */
	public function setLifeModifier(?string $lifeModifier) : ScryfallApiCard
	{
		$this->_lifeModifier = $lifeModifier;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getLifeModifier()
	 */
	public function getLifeModifier() : ?string
	{
		return $this->_lifeModifier;
	}
	
	/**
	 * Sets the loyalty of this card.
	 * 
	 * @param ?string $loyalty
	 * @return ScryfallApiCard
	 */
	public function setLoyalty(?string $loyalty) : ScryfallApiCard
	{
		$this->_loyalty = $loyalty;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getLoyalty()
	 */
	public function getLoyalty() : ?string
	{
		return $this->_loyalty;
	}
	
	/**
	 * Sets the mana cost of this card.
	 * 
	 * @param ?string $manaCost
	 * @return ScryfallApiCard
	 */
	public function setManaCost(?string $manaCost) : ScryfallApiCard
	{
		$this->_manaCost = $manaCost;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getManaCost()
	 */
	public function getManaCost() : ?string
	{
		return $this->_manaCost;
	}
	
	/**
	 * Sets the name of this card.
	 * 
	 * @param ?string $name
	 * @return ScryfallApiCard
	 */
	public function setName(?string $name) : ScryfallApiCard
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getName()
	 */
	public function getName() : ?string
	{
		return $this->_name;
	}
	
	/**
	 * Sets whether this card is non foil.
	 * 
	 * @param ?boolean $isNonFoil
	 * @return ScryfallApiCard
	 */
	public function setNonfoil(?bool $isNonFoil) : ScryfallApiCard
	{
		$this->_nonfoil = $isNonFoil;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isNonfoil()
	 */
	public function isNonfoil() : ?bool
	{
		return $this->_nonfoil;
	}
	
	/**
	 * Sets the oracle text of this card.
	 * 
	 * @param ?string $text
	 * @return ScryfallApiCard
	 */
	public function setOracleText(?string $text) : ScryfallApiCard
	{
		$this->_oracleText = $text;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getOracleText()
	 */
	public function getOracleText() : ?string
	{
		return $this->_oracleText;
	}
	
	/**
	 * Sets whether this card is oversized.
	 * 
	 * @param ?boolean $isOversized
	 * @return ScryfallApiCard
	 */
	public function setOversized(?bool $isOversized) : ScryfallApiCard
	{
		$this->_oversized = $isOversized;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isOversized()
	 */
	public function isOversized() : ?bool
	{
		return $this->_oversized;
	}
	
	/**
	 * This card’s rank/popularity on Penny Dreadful. Not all cards are ranked.
	 * 
	 * @param ?integer $rank
	 * @return ScryfallApiCard
	 */
	public function setPennyRank(?int $rank) : ScryfallApiCard
	{
		$this->_pennyRank = $rank;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPennyRank()
	 */
	public function getPennyRank() : ?int
	{
		return $this->_pennyRank;
	}
	
	/**
	 * Sets the power of this card.
	 * 
	 * @param ?string $power
	 * @return ScryfallApiCard
	 */
	public function setPower(?string $power) : ScryfallApiCard
	{
		$this->_power = $power;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPower()
	 */
	public function getPower() : ?string
	{
		return $this->_power;
	}
	
	/**
	 * Sets whether this card is on the reserved list.
	 * 
	 * @param ?boolean $isReserved
	 * @return ScryfallApiCard
	 */
	public function setReserved(?bool $isReserved) : ScryfallApiCard
	{
		$this->_reserved = $isReserved;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isReserved()
	 */
	public function isReserved() : ?bool
	{
		return $this->_reserved;
	}
	
	/**
	 * Sets the security stamp of this card.
	 * 
	 * @param ?string $stamp
	 * @return ScryfallApiCard
	 */
	public function setSecurityStamp(?string $stamp) : ScryfallApiCard
	{
		$this->_securityStamp = $stamp;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getSecurityStamp()
	 */
	public function getSecurityStamp() : ?string
	{
		return $this->_securityStamp;
	}
	
	/**
	 * Sets the toughness of this card.
	 * 
	 * @param ?string $toughness
	 * @return ScryfallApiCard
	 */
	public function setToughness(?string $toughness) : ScryfallApiCard
	{
		$this->_toughness = $toughness;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getToughness()
	 */
	public function getToughness() : ?string
	{
		return $this->_toughness;
	}
	
	/**
	 * Sets the typeline of this card.
	 * 
	 * @param ?string $typeline
	 * @return ScryfallApiCard
	 */
	public function setTypeLine(?string $typeline) : ScryfallApiCard
	{
		$this->_typeLine = $typeline;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getTypeLine()
	 */
	public function getTypeLine() : ?string
	{
		return $this->_typeLine;
	}
	
	/**
	 * Sets the artist of this card.
	 * 
	 * @param ?string $artist
	 * @return ScryfallApiCard
	 */
	public function setArtist(?string $artist) : ScryfallApiCard
	{
		$this->_artist = $artist;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getArtist()
	 */
	public function getArtist() : ?string
	{
		return $this->_artist;
	}
	
	/**
	 * Sets the ids of the artists.
	 * 
	 * @param array<integer, string> $artistIds
	 * @return ScryfallApiCard
	 */
	public function setArtistIds(array $artistIds) : ScryfallApiCard
	{
		$this->_artistIds = $artistIds;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getArtistIds()
	 */
	public function getArtistIds() : array
	{
		return $this->_artistIds;
	}
	
	/**
	 * Sets whether this card appears in boosters.
	 * 
	 * @param ?boolean $inBooster
	 * @return ScryfallApiCard
	 */
	public function setBooster(?bool $inBooster) : ScryfallApiCard
	{
		$this->_booster = $inBooster;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::hasBooster()
	 */
	public function hasBooster() : ?bool
	{
		return $this->_booster;
	}
	
	/**
	 * Sets the border color of this card.
	 * 
	 * @param ?string $color
	 * @return ScryfallApiCard
	 */
	public function setBorderColor(?string $color) : ScryfallApiCard
	{
		$this->_borderColor = $color;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getBorderColor()
	 */
	public function getBorderColor() : ?string
	{
		return $this->_borderColor;
	}
	
	/**
	 * Sets the uuid of the back of this card.
	 * 
	 * @param UuidInterface $uuid
	 * @return ScryfallApiCard
	 */
	public function setCardBackId(?UuidInterface $uuid) : ScryfallApiCard
	{
		$this->_cardBackId = $uuid;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getCardBackId()
	 */
	public function getCardBackId() : ?UuidInterface
	{
		return $this->_cardBackId;
	}
	
	/**
	 * Sets the collector number of this card.
	 * 
	 * @param ?string $number
	 * @return ScryfallApiCard
	 */
	public function setCollectorNumber(?string $number) : ScryfallApiCard
	{
		$this->_collectorNumber = $number;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getCollectorNumber()
	 */
	public function getCollectorNumber() : ?string
	{
		return $this->_collectorNumber;
	}
	
	/**
	 * Sets whether this card is digital.
	 * 
	 * @param ?boolean $isDigital
	 * @return ScryfallApiCard
	 */
	public function setDigital(?bool $isDigital) : ScryfallApiCard
	{
		$this->_digital = $isDigital;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isDigital()
	 */
	public function isDigital() : ?bool
	{
		return $this->_digital;
	}
	
	/**
	 * Sets the flavor text of this card.
	 * 
	 * @param ?string $text
	 * @return ScryfallApiCard
	 */
	public function setFlavorText(?string $text) : ScryfallApiCard
	{
		$this->_flavorText = $text;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getFlavorText()
	 */
	public function getFlavorText() : ?string
	{
		return $this->_flavorText;
	}
	
	/**
	 * Sets the frame effects of this card.
	 * 
	 * @param array<integer, string> $effects
	 * @return ScryfallApiCard
	 */
	public function setFrameEffects(array $effects) : ScryfallApiCard
	{
		$this->_frameEffects = $effects;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getFrameEffects()
	 */
	public function getFrameEffects() : array
	{
		return $this->_frameEffects;
	}
	
	/**
	 * Sets the frame of this card.
	 * 
	 * @param ?string $frame
	 * @return ScryfallApiCard
	 */
	public function setFrame(?string $frame) : ScryfallApiCard
	{
		$this->_frame = $frame;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getFrame()
	 */
	public function getFrame() : ?string
	{
		return $this->_frame;
	}
	
	/**
	 * Sets whether this card is full art.
	 * 
	 * @param ?boolean $fullArt
	 * @return ScryfallApiCard
	 */
	public function setFullArt(?bool $fullArt) : ScryfallApiCard
	{
		$this->_fullArt = $fullArt;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isFullArt()
	 */
	public function isFullArt() : ?bool
	{
		return $this->_fullArt;
	}
	
	/**
	 * Sets the games where this card is available.
	 * 
	 * @param array<integer, string> $games
	 * @return ScryfallApiCard
	 */
	public function setGames(array $games) : ScryfallApiCard
	{
		$this->_games = $games;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getGames()
	 */
	public function getGames() : array
	{
		return $this->_games;
	}
	
	/**
	 * Sets whether this card has highres image.
	 * 
	 * @param ?boolean $isHighres
	 * @return ScryfallApiCard
	 */
	public function setHighresImage(?bool $isHighres) : ScryfallApiCard
	{
		$this->_highresImage = $isHighres;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isHighresImage()
	 */
	public function isHighresImage() : ?bool
	{
		return $this->_highresImage;
	}
	
	/**
	 * Sets the image status of this card.
	 * 
	 * @param ?string $status
	 * @return ScryfallApiCard
	 */
	public function setImageStatus(?string $status) : ScryfallApiCard
	{
		$this->_imageStatus = $status;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getImageStatus()
	 */
	public function getImageStatus() : ?string
	{
		return $this->_imageStatus;
	}
	
	/**
	 * Sets the illustration id of this card.
	 * 
	 * @param ?UuidInterface $uuid
	 * @return ScryfallApiCard
	 */
	public function setIllustrationId(?UuidInterface $uuid) : ScryfallApiCard
	{
		$this->_illustrationId = $uuid;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getIllustrationId()
	 */
	public function getIllustrationId() : ?UuidInterface
	{
		return $this->_illustrationId;
	}
	
	/**
	 * Sets the imagery for this card.
	 * 
	 * @param ?ScryfallApiCardImageryInterface $imagery
	 * @return ScryfallApiCard
	 */
	public function setImageUris(?ScryfallApiCardImageryInterface $imagery) : ScryfallApiCard
	{
		$this->_imageUris = $imagery;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getImageUris()
	 */
	public function getImageUris() : ?ScryfallApiCardImageryInterface
	{
		return $this->_imageUris;
	}
	
	/**
	 * Sets the printed name of this card.
	 * 
	 * @param ?string $name
	 * @return ScryfallApiCard
	 */
	public function setPrintedName(?string $name) : ScryfallApiCard
	{
		$this->_printedName = $name;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPrintedName()
	 */
	public function getPrintedName() : ?string
	{
		return $this->_printedName;
	}
	
	/**
	 * Sets the printed text of this card.
	 * 
	 * @param ?string $text
	 * @return ScryfallApiCard
	 */
	public function setPrintedText(?string $text) : ScryfallApiCard
	{
		$this->_printedText = $text;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPrintedText()
	 */
	public function getPrintedText() : ?string
	{
		return $this->_printedText;
	}
	
	/**
	 * Sets the printed typeline of this card.
	 * 
	 * @param ?string $typeline
	 * @return ScryfallApiCard
	 */
	public function setPrintedTypeLine(?string $typeline) : ScryfallApiCard
	{
		$this->_printedTypeLine = $typeline;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPrintedTypeLine()
	 */
	public function getPrintedTypeLine() : ?string
	{
		return $this->_printedTypeLine;
	}
	
	/**
	 * Sets whether this card is promotional.
	 * 
	 * @param ?boolean $isPromo
	 * @return ScryfallApiCard
	 */
	public function setPromo(?bool $isPromo) : ScryfallApiCard
	{
		$this->_promo = $isPromo;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isPromo()
	 */
	public function isPromo() : ?bool
	{
		return $this->_promo;
	}
	
	/**
	 * Sets the promo types of this card.
	 * 
	 * @param array<integer, string> $types
	 * @return ScryfallApiCard
	 */
	public function setPromoTypes(array $types) : ScryfallApiCard
	{
		$this->_promoTypes = $types;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPromoTypes()
	 */
	public function getPromoTypes() : array
	{
		return $this->_promoTypes;
	}
	
	/**
	 * Sets the purchase uris of this card.
	 * 
	 * @param ?ScryfallApiCardPurchaseInterface $purchase
	 * @return ScryfallApiCard
	 */
	public function setPurchaseUris(?ScryfallApiCardPurchaseInterface $purchase) : ScryfallApiCard
	{
		$this->_purchaseUris = $purchase;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPurchaseUris()
	 */
	public function getPurchaseUris() : ?ScryfallApiCardPurchaseInterface
	{
		return $this->_purchaseUris;
	}
	
	/**
	 * Sets the rarity of this card.
	 * 
	 * @param ?string $rarity
	 * @return ScryfallApiCard
	 */
	public function setRarity(?string $rarity) : ScryfallApiCard
	{
		$this->_rarity = $rarity;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getRarity()
	 */
	public function getRarity() : ?string
	{
		return $this->_rarity;
	}
	
	/**
	 * Sets the related resources for this card.
	 * 
	 * @param ?ScryfallApiCardResourceInterface $resources
	 * @return ScryfallApiCard
	 */
	public function setRelatedUris(?ScryfallApiCardResourceInterface $resources) : ScryfallApiCard
	{
		$this->_relatedUris = $resources;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getRelatedUris()
	 */
	public function getRelatedUris() : ?ScryfallApiCardResourceInterface
	{
		return $this->_relatedUris;
	}
	
	/**
	 * Sets whether the prices for this card.
	 * 
	 * @param ?ScryfallApiPricesInterface $prices
	 * @return ScryfallApiCard
	 */
	public function setPrices(?ScryfallApiPricesInterface $prices) : ScryfallApiCard
	{
		$this->_prices = $prices;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPrices()
	 */
	public function getPrices() : ?ScryfallApiPricesInterface
	{
		return $this->_prices;
	}
	
	/**
	 * Sets when this card was released.
	 * 
	 * @param ?DateTimeInterface $datetime
	 * @return ScryfallApiCard
	 */
	public function setReleasedAt(?DateTimeInterface $datetime) : ScryfallApiCard
	{
		$this->_releasedAt = $datetime;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getReleasedAt()
	 */
	public function getReleasedAt() : ?DateTimeInterface
	{
		return $this->_releasedAt;
	}
	
	/**
	 * Sets whether this card is a reprint.
	 * 
	 * @param ?boolean $isReprint
	 * @return ScryfallApiCard
	 */
	public function setReprint(?bool $isReprint) : ScryfallApiCard
	{
		$this->_reprint = $isReprint;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isReprint()
	 */
	public function isReprint() : ?bool
	{
		return $this->_reprint;
	}
	
	/**
	 * Sets the scryfall set uri.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCard
	 */
	public function setScryfallSetUri(?UriInterface $uri) : ScryfallApiCard
	{
		$this->_scryfallSetUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getScryfallSetUri()
	 */
	public function getScryfallSetUri() : ?UriInterface
	{
		return $this->_scryfallSetUri;
	}
	
	/**
	 * Sets the set id.
	 * 
	 * @param ?string $setid
	 * @return ScryfallApiCard
	 */
	public function setSetId(?string $setid) : ScryfallApiCard
	{
		$this->_setId = $setid;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getSetId()
	 */
	public function getSetId() : ?string
	{
		return $this->_setId;
	}
	
	/**
	 * Sets the set name of this set.
	 * 
	 * @param ?string $set
	 * @return ScryfallApiCard
	 */
	public function setSet(?string $set) : ScryfallApiCard
	{
		$this->_set = $set;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getSet()
	 */
	public function getSet() : ?string
	{
		return $this->_set;
	}
	
	/**
	 * Sets the set name of this card.
	 * 
	 * @param ?string $name
	 * @return ScryfallApiCard
	 */
	public function setSetName(?string $name) : ScryfallApiCard
	{
		$this->_setName = $name;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getSetName()
	 */
	public function getSetName() : ?string
	{
		return $this->_setName;
	}
	
	/**
	 * Sets the set search uri.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCard
	 */
	public function setSetSearchUri(?UriInterface $uri) : ScryfallApiCard
	{
		$this->_setSearchUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getSetSearchUri()
	 */
	public function getSetSearchUri() : ?UriInterface
	{
		return $this->_setSearchUri;
	}
	
	/**
	 * Sets the set type of this card.
	 * 
	 * @param ?string $type
	 * @return ScryfallApiCard
	 */
	public function setSetType(?string $type) : ScryfallApiCard
	{
		$this->_setType = $type;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getSetType()
	 */
	public function getSetType() : ?string
	{
		return $this->_setType;
	}
	
	/**
	 * Sets the set uri of this card.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCard
	 */
	public function setSetUri(?UriInterface $uri) : ScryfallApiCard
	{
		$this->_setUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getSetUri()
	 */
	public function getSetUri() : ?UriInterface
	{
		return $this->_setUri;
	}
	
	/**
	 * Sets whether this card is story spotlight.
	 * 
	 * @param ?boolean $storySpotlight
	 * @return ScryfallApiCard
	 */
	public function setStorySpotlight(?bool $storySpotlight) : ScryfallApiCard
	{
		$this->_storySpotlight = $storySpotlight;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isStorySpotlight()
	 */
	public function isStorySpotlight() : ?bool
	{
		return $this->_storySpotlight;
	}
	
	/**
	 * Sets whether this card is textless.
	 * 
	 * @param ?boolean $textless
	 * @return ScryfallApiCard
	 */
	public function setTextless(?bool $textless) : ScryfallApiCard
	{
		$this->_textless = $textless;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isTextless()
	 */
	public function isTextless() : ?bool
	{
		return $this->_textless;
	}
	
	/**
	 * Sets whether this card is a variation.
	 * 
	 * @param ?boolean $variation
	 * @return ScryfallApiCard
	 */
	public function setVariation(?bool $variation) : ScryfallApiCard
	{
		$this->_variation = $variation;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::isVariation()
	 */
	public function isVariation() : ?bool
	{
		return $this->_variation;
	}
	
	/**
	 * Sets the variation of this card.
	 * 
	 * @param ?UuidInterface $uuid
	 * @return ScryfallApiCard
	 */
	public function setVariationOf(?UuidInterface $uuid) : ScryfallApiCard
	{
		$this->_variationOf = $uuid;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getVariationOf()
	 */
	public function getVariationOf() : ?UuidInterface
	{
		return $this->_variationOf;
	}
	
	/**
	 * Sets the watermark of this card.
	 * 
	 * @param ?string $watermark
	 * @return ScryfallApiCard
	 */
	public function setWatermark(?string $watermark) : ScryfallApiCard
	{
		$this->_watermark = $watermark;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getWatermark()
	 */
	public function getWatermark() : ?string
	{
		return $this->_watermark;
	}
	
	/**
	 * Sets the preview of this card.
	 * 
	 * @param ?ScryfallApiPreviewInterface $preview
	 * @return ScryfallApiCard
	 */
	public function setPreview(?ScryfallApiPreviewInterface $preview) : ScryfallApiCard
	{
		$this->_preview = $preview;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardInterface::getPreview()
	 */
	public function getPreview() : ?ScryfallApiPreviewInterface
	{
		return $this->_preview;
	}
	
}
