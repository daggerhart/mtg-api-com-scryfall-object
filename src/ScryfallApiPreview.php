<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiPreview class file.
 * 
 * This class represents the preview data of a given card.
 * 
 * @author Anastaszor
 */
class ScryfallApiPreview implements ScryfallApiPreviewInterface
{
	
	/**
	 * The date this card was previewed.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected $_previewedAt;
	
	/**
	 * A link to the preview for this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_sourceUri;
	
	/**
	 * The name of the source that previewed this card.
	 * 
	 * @var ?string
	 */
	protected $_source;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL PREVIEW';
	}
	
	/**
	 * Sets the date when this was previewed.
	 * 
	 * @param ?DateTimeInterface $datetime
	 * @return ScryfallApiPreview
	 */
	public function setPreviewedAt(?DateTimeInterface $datetime) : ScryfallApiPreview
	{
		$this->_previewedAt = $datetime;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPreviewInterface::getPreviewedAt()
	 */
	public function getPreviewedAt() : ?DateTimeInterface
	{
		return $this->_previewedAt;
	}
	
	/**
	 * Sets the uri of the source.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiPreview
	 */
	public function setSourceUri(?UriInterface $uri) : ScryfallApiPreview
	{
		$this->_sourceUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPreviewInterface::getSourceUri()
	 */
	public function getSourceUri() : ?UriInterface
	{
		return $this->_sourceUri;
	}
	
	/**
	 * Sets the source of the preview.
	 * 
	 * @param ?string $source
	 * @return ScryfallApiPreview
	 */
	public function setSource(?string $source) : ScryfallApiPreview
	{
		$this->_source = $source;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPreviewInterface::getSource()
	 */
	public function getSource() : ?string
	{
		return $this->_source;
	}
	
}
