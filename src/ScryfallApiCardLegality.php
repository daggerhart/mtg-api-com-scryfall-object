<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

/**
 * ScryfallApiCardLegality class file.
 * 
 * This class represents the different legality statuses of this card in the
 * different formats.
 * 
 * Possible legalities are "legal", "not_legal", "restricted", and "banned".
 * 
 * @author Anastaszor
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class ScryfallApiCardLegality implements ScryfallApiCardLegalityInterface
{
	
	/**
	 * The legality for 1v1.
	 * 
	 * @var ?string
	 */
	protected $_oneVone;
	
	/**
	 * The legality for alchemy.
	 * 
	 * @var ?string
	 */
	protected $_alchemy;
	
	/**
	 * The legality for brawl.
	 * 
	 * @var ?string
	 */
	protected $_brawl;
	
	/**
	 * The legality for commander.
	 * 
	 * @var ?string
	 */
	protected $_commander;
	
	/**
	 * The legality for duel commander.
	 * 
	 * @var ?string
	 */
	protected $_duel;
	
	/**
	 * The legality for explorer.
	 * 
	 * @var ?string
	 */
	protected $_explorer;
	
	/**
	 * The legality for frontier.
	 * 
	 * @var ?string
	 */
	protected $_frontier;
	
	/**
	 * The legality for future.
	 * 
	 * @var ?string
	 */
	protected $_future;
	
	/**
	 * The legality for gladiator.
	 * 
	 * @var ?string
	 */
	protected $_gladiator;
	
	/**
	 * The legality for historic.
	 * 
	 * @var ?string
	 */
	protected $_historic;
	
	/**
	 * The legality for historic brawl.
	 * 
	 * @var ?string
	 */
	protected $_historicBrawl;
	
	/**
	 * The legality for legacy.
	 * 
	 * @var ?string
	 */
	protected $_legacy;
	
	/**
	 * The legality for modern.
	 * 
	 * @var ?string
	 */
	protected $_modern;
	
	/**
	 * The legality for oldschool format.
	 * 
	 * @var ?string
	 */
	protected $_oldschool;
	
	/**
	 * The legality for pauper.
	 * 
	 * @var ?string
	 */
	protected $_pauper;
	
	/**
	 * The legality for pauper commander.
	 * 
	 * @var ?string
	 */
	protected $_pauperCommander;
	
	/**
	 * The legality for penny.
	 * 
	 * @var ?string
	 */
	protected $_penny;
	
	/**
	 * The legality for pioneer.
	 * 
	 * @var ?string
	 */
	protected $_pioneer;
	
	/**
	 * The legality for premodern.
	 * 
	 * @var ?string
	 */
	protected $_premodern;
	
	/**
	 * The legality for standard.
	 * 
	 * @var ?string
	 */
	protected $_standard;
	
	/**
	 * The legality for vintage.
	 * 
	 * @var ?string
	 */
	protected $_vintage;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL CARD LEGALITY';
	}
	
	/**
	 * Sets the legality to 1v1.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function set1v1(?string $legal) : ScryfallApiCardLegality
	{
		$this->_oneVone = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::get1v1()
	 */
	public function get1v1() : ?string
	{
		return $this->_oneVone;
	}
	
	/**
	 * Sets the legality to alchemy.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setAlchemy(?string $legal) : ScryfallApiCardLegality
	{
		$this->_alchemy = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getAlchemy()
	 */
	public function getAlchemy() : ?string
	{
		return $this->_alchemy;
	}
	
	/**
	 * Sets the legality to brawl.
	 * 
	 * @param string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setBrawl(?string $legal) : ScryfallApiCardLegality
	{
		$this->_brawl = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getBrawl()
	 */
	public function getBrawl() : ?string
	{
		return $this->_brawl;
	}
	
	/**
	 * Sets the legality for commander.
	 * 
	 * @param ?string $commander
	 * @return ScryfallApiCardLegality
	 */
	public function setCommander(?string $commander) : ScryfallApiCardLegality
	{
		$this->_commander = $commander;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getCommander()
	 */
	public function getCommander() : ?string
	{
		return $this->_commander;
	}
	
	/**
	 * Sets the legality for duel.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setDuel(?string $legal) : ScryfallApiCardLegality
	{
		$this->_duel = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getDuel()
	 */
	public function getDuel() : ?string
	{
		return $this->_duel;
	}
	
	/**
	 * Sets the legality for frontier.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setExplorer(?string $legal) : ScryfallApiCardLegality
	{
		$this->_explorer = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getExplorer()
	 */
	public function getExplorer() : ?string
	{
		return $this->_explorer;
	}
	
	/**
	 * Sets the legality for frontier.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setFrontier(?string $legal) : ScryfallApiCardLegality
	{
		$this->_frontier = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getFrontier()
	 */
	public function getFrontier() : ?string
	{
		return $this->_frontier;
	}
	
	/**
	 * Sets the legality for future.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setFuture(?string $legal) : ScryfallApiCardLegality
	{
		$this->_future = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getFuture()
	 */
	public function getFuture() : ?string
	{
		return $this->_future;
	}
	
	/**
	 * Sets the legality for gladiator.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setGladiator(?string $legal) : ScryfallApiCardLegality
	{
		$this->_gladiator = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getGladiator()
	 */
	public function getGladiator() : ?string
	{
		return $this->_gladiator;
	}
	
	/**
	 * Sets the legality for historic.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setHistoric(?string $legal) : ScryfallApiCardLegality
	{
		$this->_historic = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getHistoric()
	 */
	public function getHistoric() : ?string
	{
		return $this->_historic;
	}
	
	/**
	 * Sets the legality for historic brawl.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setHistoricbrawl(?string $legal) : ScryfallApiCardLegality
	{
		$this->_historicBrawl = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getHistoricBrawl()
	 */
	public function getHistoricBrawl() : ?string
	{
		return $this->_historicBrawl;
	}
	
	/**
	 * Sets the legality for legacy.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setLegacy(?string $legal) : ScryfallApiCardLegality
	{
		$this->_legacy = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getLegacy()
	 */
	public function getLegacy() : ?string
	{
		return $this->_legacy;
	}
	
	/**
	 * Sets the legality for modern.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setModern(?string $legal) : ScryfallApiCardLegality
	{
		$this->_modern = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getModern()
	 */
	public function getModern() : ?string
	{
		return $this->_modern;
	}
	
	/**
	 * Sets the legality for oldschool.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setOldschool(?string $legal) : ScryfallApiCardLegality
	{
		$this->_oldschool = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getOldSchool()
	 */
	public function getOldSchool() : ?string
	{
		return $this->_oldschool;
	}
	
	/**
	 * Sets the legality for pauper.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setPauper(?string $legal) : ScryfallApiCardLegality
	{
		$this->_pauper = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getPauper()
	 */
	public function getPauper() : ?string
	{
		return $this->_pauper;
	}
	
	/**
	 * Sets the legality for pauper commander.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setPaupercommander(?string $legal) : ScryfallApiCardLegality
	{
		$this->_pauperCommander = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getPauperCommander()
	 */
	public function getPauperCommander() : ?string
	{
		return $this->_pauperCommander;
	}
	
	/**
	 * Sets the legality for penny.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setPenny(?string $legal) : ScryfallApiCardLegality
	{
		$this->_penny = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getPenny()
	 */
	public function getPenny() : ?string
	{
		return $this->_penny;
	}
	
	/**
	 * Sets the legality for pioneer.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setPioneer(?string $legal) : ScryfallApiCardLegality
	{
		$this->_pioneer = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getPioneer()
	 */
	public function getPioneer() : ?string
	{
		return $this->_pioneer;
	}
	
	/**
	 * Sets the legality for premodern.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setPremodern(?string $legal) : ScryfallApiCardLegality
	{
		$this->_premodern = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getPremodern()
	 */
	public function getPremodern() : ?string
	{
		return $this->_premodern;
	}
	
	/**
	 * Sets the legality for standard.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setStandard(?string $legal) : ScryfallApiCardLegality
	{
		$this->_standard = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getStandard()
	 */
	public function getStandard() : ?string
	{
		return $this->_standard;
	}
	
	/**
	 * Sets the legality for vintage.
	 * 
	 * @param ?string $legal
	 * @return ScryfallApiCardLegality
	 */
	public function setVintage(?string $legal) : ScryfallApiCardLegality
	{
		$this->_vintage = $legal;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardLegalityInterface::getVintage()
	 */
	public function getVintage() : ?string
	{
		return $this->_vintage;
	}
	
}
