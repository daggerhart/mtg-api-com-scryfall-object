<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

/**
 * ScryfallApiParsedMana class file.
 * 
 * This class represents the parsed mana response that the api has understood.
 * 
 * @author Anastaszor
 */
class ScryfallApiParsedMana implements ScryfallApiParsedManaInterface
{
	
	/**
	 * The normalized cost, with correctly-ordered and wrapped mana symbols.
	 * 
	 * @var ?string
	 */
	protected $_cost;
	
	/**
	 * The converted mana cost. If you submit Un-set mana symbols, this decimal
	 * could include fractional parts.
	 * 
	 * @var ?float
	 */
	protected $_cmc;
	
	/**
	 * The colors of the given cost.
	 * 
	 * @var array<integer, string>
	 */
	protected $_colors = [];
	
	/**
	 * True if the cost is colorless.
	 * 
	 * @var ?boolean
	 */
	protected $_colorless;
	
	/**
	 * True if the cost is monocolored.
	 * 
	 * @var ?boolean
	 */
	protected $_monocolored;
	
	/**
	 * True if the cost is multicolored.
	 * 
	 * @var ?boolean
	 */
	protected $_multicolored;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL PARSED MANA';
	}
	
	/**
	 * Sets the cost value of this mana.
	 * 
	 * @param ?string $cost
	 * @return ScryfallApiParsedMana
	 */
	public function setCost(?string $cost) : ScryfallApiParsedMana
	{
		$this->_cost = $cost;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiParsedManaInterface::getCost()
	 */
	public function getCost() : ?string
	{
		return $this->_cost;
	}
	
	/**
	 * Sets the cmc of this mana.
	 * 
	 * @param ?float $cmc
	 * @return ScryfallApiParsedMana
	 */
	public function setCmc(?float $cmc) : ScryfallApiParsedMana
	{
		$this->_cmc = $cmc;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiParsedManaInterface::getCmc()
	 */
	public function getCmc() : ?float
	{
		return $this->_cmc;
	}
	
	/**
	 * Sets the colors of this mana.
	 * 
	 * @param array<integer, string> $colors
	 * @return ScryfallApiParsedMana
	 */
	public function setColors(array $colors) : ScryfallApiParsedMana
	{
		$this->_colors = $colors;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiParsedManaInterface::getColors()
	 */
	public function getColors() : array
	{
		return $this->_colors;
	}
	
	/**
	 * Sets whether this mana is colorless.
	 * 
	 * @param ?boolean $isColorless
	 * @return ScryfallApiParsedMana
	 */
	public function setColorless(?bool $isColorless) : ScryfallApiParsedMana
	{
		$this->_colorless = $isColorless;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiParsedManaInterface::isColorless()
	 */
	public function isColorless() : ?bool
	{
		return $this->_colorless;
	}
	
	/**
	 * Sets whether this mana is monocolored.
	 * 
	 * @param ?boolean $isMonocolored
	 * @return ScryfallApiParsedMana
	 */
	public function setMonocolored(?bool $isMonocolored) : ScryfallApiParsedMana
	{
		$this->_monocolored = $isMonocolored;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiParsedManaInterface::isMonocolored()
	 */
	public function isMonocolored() : ?bool
	{
		return $this->_monocolored;
	}
	
	/**
	 * Sets whether this mana is multicolored.
	 * 
	 * @param ?boolean $isMulticolored
	 * @return ScryfallApiParsedMana
	 */
	public function setMulticolored(?bool $isMulticolored) : ScryfallApiParsedMana
	{
		$this->_multicolored = $isMulticolored;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiParsedManaInterface::isMulticolored()
	 */
	public function isMulticolored() : ?bool
	{
		return $this->_multicolored;
	}
	
}
