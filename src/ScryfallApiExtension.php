<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiExtension class file.
 * 
 * This class represents a set from the scryfall api.
 * 
 * @author Anastaszor
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class ScryfallApiExtension implements ScryfallApiExtensionInterface
{
	
	/**
	 * A unique ID for this set on Scryfall that will not change.
	 * 
	 * @var UuidInterface
	 */
	protected $_id;
	
	/**
	 * The unique three to five-letter code for this set. 
	 * 
	 * @var ?string
	 */
	protected $_code;
	
	/**
	 * The unique code for this set on MTGO, which may differ from the regular code. 
	 * 
	 * @var ?string
	 */
	protected $_mtgoCode;
	
	/**
	 * The arena code.
	 * 
	 * @var ?string
	 */
	protected $_arenaCode;
	
	/**
	 * This set’s ID on TCGplayer’s API, also known as the groupId.
	 * 
	 * @var ?integer
	 */
	protected $_tcgplayerId;
	
	/**
	 * The English name of the set.
	 * 
	 * @var ?string
	 */
	protected $_name;
	
	/**
	 * A computer-readable classification for this set. 
	 * 
	 * @var ?string
	 */
	protected $_setType;
	
	/**
	 * The date the set was released or the first card was printed in the set
	 * (in GMT-8 Pacific time). 
	 * 
	 * @var ?DateTimeInterface
	 */
	protected $_releasedAt;
	
	/**
	 * The block code for this set, if any.
	 * 
	 * @var ?string
	 */
	protected $_blockCode;
	
	/**
	 * The block or group name code for this set, if any.
	 * 
	 * @var ?string
	 */
	protected $_block;
	
	/**
	 * The set code for the parent set, if any. promo and token sets often have
	 * a parent set.
	 * 
	 * @var ?string
	 */
	protected $_parentSetCode;
	
	/**
	 * The number of cards in this set.
	 * 
	 * @var ?integer
	 */
	protected $_cardCount;
	
	/**
	 * True if this set was only released on Magic Online.
	 * 
	 * @var ?boolean
	 */
	protected $_digital;
	
	/**
	 * True if this set contains only foil cards.
	 * 
	 * @var ?boolean
	 */
	protected $_foilOnly;
	
	/**
	 * True if this set contains only nonfoil cards.
	 * 
	 * @var ?boolean
	 */
	protected $_nonfoilOnly;
	
	/**
	 * A link to this set’s permapage on Scryfall’s website.
	 * 
	 * @var ?UriInterface
	 */
	protected $_scryfallUri;
	
	/**
	 * A link to this set object on Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	protected $_uri;
	
	/**
	 * A URI to an SVG file for this set’s icon on Scryfall’s CDN. Hotlinking
	 * this image isn’t recommended, because it may change slightly over time.
	 * You should download it and use it locally for your particular user
	 * interface needs. 
	 * 
	 * @var ?UriInterface
	 */
	protected $_iconSvgUri;
	
	/**
	 * A Scryfall API URI that you can request to begin paginating over the
	 * cards in this set. 
	 * 
	 * @var ?UriInterface
	 */
	protected $_searchUri;
	
	/**
	 * Builds a new ScryfallApiExtension with its data.
	 * 
	 * @param UuidInterface $id
	 */
	public function __construct(UuidInterface $id)
	{
		$this->_id = $id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL EXTENSION '.$this->_id->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getId()
	 */
	public function getId() : UuidInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the code of this extension.
	 * 
	 * @param ?string $code
	 * @return ScryfallApiExtension
	 */
	public function setCode(?string $code) : ScryfallApiExtension
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getCode()
	 */
	public function getCode() : ?string
	{
		return $this->_code;
	}
	
	/**
	 * Sets the mtgo code of this extension.
	 * 
	 * @param ?string $code
	 * @return ScryfallApiExtension
	 */
	public function setMtgoCode(?string $code) : ScryfallApiExtension
	{
		$this->_mtgoCode = $code;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getMtgoCode()
	 */
	public function getMtgoCode() : ?string
	{
		return $this->_mtgoCode;
	}
	
	/**
	 * Sets the arena code of this set.
	 * 
	 * @param ?string $code
	 * @return ScryfallApiExtension
	 */
	public function setArenaCode(?string $code) : ScryfallApiExtension
	{
		$this->_arenaCode = $code;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getArenaCode()
	 */
	public function getArenaCode() : ?string
	{
		return $this->_arenaCode;
	}
	
	/**
	 * Sets the tcgplayer id of this extension.
	 * 
	 * @param ?integer $tcgpid
	 * @return ScryfallApiExtension
	 */
	public function setTcgplayerId(?int $tcgpid) : ScryfallApiExtension
	{
		$this->_tcgplayerId = $tcgpid;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getTcgplayerId()
	 */
	public function getTcgplayerId() : ?int
	{
		return $this->_tcgplayerId;
	}
	
	/**
	 * Sets the name of this extension.
	 * 
	 * @param ?string $name
	 * @return ScryfallApiExtension
	 */
	public function setName(?string $name) : ScryfallApiExtension
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getName()
	 */
	public function getName() : ?string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the type of this extension.
	 * 
	 * @param ?string $type
	 * @return ScryfallApiExtension
	 */
	public function setSetType(?string $type) : ScryfallApiExtension
	{
		$this->_setType = $type;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getSetType()
	 */
	public function getSetType() : ?string
	{
		return $this->_setType;
	}
	
	/**
	 * Sets when this extension was released.
	 * 
	 * @param DateTimeInterface $datetime
	 * @return ScryfallApiExtension
	 */
	public function setReleasedAt(?DateTimeInterface $datetime) : ScryfallApiExtension
	{
		$this->_releasedAt = $datetime;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getReleasedAt()
	 */
	public function getReleasedAt() : ?DateTimeInterface
	{
		return $this->_releasedAt;
	}
	
	/**
	 * Sets the code of the block of this extension.
	 * 
	 * @param ?string $code
	 * @return ScryfallApiExtension
	 */
	public function setBlockCode(?string $code) : ScryfallApiExtension
	{
		$this->_blockCode = $code;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getBlockCode()
	 */
	public function getBlockCode() : ?string
	{
		return $this->_blockCode;
	}
	
	/**
	 * Sets the block of this extension.
	 * 
	 * @param ?string $block
	 * @return ScryfallApiExtension
	 */
	public function setBlock(?string $block) : ScryfallApiExtension
	{
		$this->_block = $block;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getBlock()
	 */
	public function getBlock() : ?string
	{
		return $this->_block;
	}
	
	/**
	 * Sets the parent set code of this extension.
	 * 
	 * @param ?string $code
	 * @return ScryfallApiExtension
	 */
	public function setParentSetCode(?string $code) : ScryfallApiExtension
	{
		$this->_parentSetCode = $code;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getParentSetCode()
	 */
	public function getParentSetCode() : ?string
	{
		return $this->_parentSetCode;
	}
	
	/**
	 * Sets the card count of this extension.
	 * 
	 * @param ?integer $cardCount
	 * @return ScryfallApiExtension
	 */
	public function setCardCount(?int $cardCount) : ScryfallApiExtension
	{
		$this->_cardCount = $cardCount;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getCardCount()
	 */
	public function getCardCount() : ?int
	{
		return $this->_cardCount;
	}
	
	/**
	 * Sets whether this extension is digital.
	 * 
	 * @param ?boolean $digital
	 * @return ScryfallApiExtension
	 */
	public function setDigital(?bool $digital) : ScryfallApiExtension
	{
		$this->_digital = $digital;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::isDigital()
	 */
	public function isDigital() : ?bool
	{
		return $this->_digital;
	}
	
	/**
	 * Sets whether this extension is foil only.
	 * 
	 * @param ?boolean $foilOnly
	 * @return ScryfallApiExtension
	 */
	public function setFoilOnly(?bool $foilOnly) : ScryfallApiExtension
	{
		$this->_foilOnly = $foilOnly;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::isFoilOnly()
	 */
	public function isFoilOnly() : ?bool
	{
		return $this->_foilOnly;
	}
	
	/**
	 * Sets whether this extension is nonfoil only.
	 * 
	 * @param ?boolean $nonfoilOnly
	 * @return ScryfallApiExtension
	 */
	public function setNonfoilOnly(?bool $nonfoilOnly) : ScryfallApiExtension
	{
		$this->_nonfoilOnly = $nonfoilOnly;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::isNonfoilOnly()
	 */
	public function isNonfoilOnly() : ?bool
	{
		return $this->_nonfoilOnly;
	}
	
	/**
	 * Sets the scryfall uri of this extension.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiExtension
	 */
	public function setScryfallUri(?UriInterface $uri) : ScryfallApiExtension
	{
		$this->_scryfallUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getScryfallUri()
	 */
	public function getScryfallUri() : ?UriInterface
	{
		return $this->_scryfallUri;
	}
	
	/**
	 * Sets the uri of this extension.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiExtension
	 */
	public function setUri(?UriInterface $uri) : ScryfallApiExtension
	{
		$this->_uri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getUri()
	 */
	public function getUri() : ?UriInterface
	{
		return $this->_uri;
	}
	
	/**
	 * Sets the icon svg uri of this extension.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiExtension
	 */
	public function setIconSvgUri(?UriInterface $uri) : ScryfallApiExtension
	{
		$this->_iconSvgUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getIconSvgUri()
	 */
	public function getIconSvgUri() : ?UriInterface
	{
		return $this->_iconSvgUri;
	}
	
	/**
	 * Sets the search uri of this extension.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiExtension
	 */
	public function setSearchUri(?UriInterface $uri) : ScryfallApiExtension
	{
		$this->_searchUri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiExtensionInterface::getSearchUri()
	 */
	public function getSearchUri() : ?UriInterface
	{
		return $this->_searchUri;
	}
	
}
