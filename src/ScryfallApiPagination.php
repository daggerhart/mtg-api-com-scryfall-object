<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use ArrayIterator;
use Iterator;

/**
 * ScryfallApiPagination class file.
 * 
 * This class represents a pagination from the scryfall api.
 * 
 * @author Anastaszor
 * @template T of object
 * @implements ScryfallApiPaginationInterface<T>
 */
class ScryfallApiPagination implements ScryfallApiPaginationInterface
{
	
	/**
	 * Whether this collection has more pages.
	 * 
	 * @var ?boolean
	 */
	protected $_hasMore;
	
	/**
	 * The next page of this collection. 
	 * 
	 * @var ?integer
	 */
	protected $_nextPage;
	
	/**
	 * The total cards of this collection.
	 * 
	 * @var ?integer
	 */
	protected $_totalCards;
	
	/**
	 * The actual data in the collection.
	 * 
	 * @var ?Iterator<integer, T>
	 */
	protected $_data;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL PAGINATION';
	}
	
	/**
	 * Sets whether this pagination has more.
	 * 
	 * @param ?boolean $hasMore
	 * @return ScryfallApiPagination<T>
	 */
	public function setHasMore(?bool $hasMore) : ScryfallApiPagination
	{
		$this->_hasMore = $hasMore;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPaginationInterface::hasMore()
	 */
	public function hasMore() : ?bool
	{
		return $this->_hasMore;
	}
	
	/**
	 * Sets the next page.
	 * 
	 * @param ?integer $nextPage
	 * @return ScryfallApiPagination<T>
	 */
	public function setNextPage(?int $nextPage) : ScryfallApiPagination
	{
		$this->_nextPage = $nextPage;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPaginationInterface::getNextPage()
	 */
	public function getNextPage() : ?int
	{
		return $this->_nextPage;
	}
	
	/**
	 * Sets the total cards.
	 * 
	 * @param ?integer $total
	 * @return ScryfallApiPagination<T>
	 */
	public function setTotalCards(?int $total) : ScryfallApiPagination
	{
		$this->_totalCards = $total;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPaginationInterface::getTotalCards()
	 */
	public function getTotalCards() : ?int
	{
		return $this->_totalCards;
	}
	
	/**
	 * Sets the data.
	 * 
	 * @param Iterator<integer, T> $data
	 * @return ScryfallApiPagination<T>
	 */
	public function setData(Iterator $data) : ScryfallApiPagination
	{
		$this->_data = $data;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPaginationInterface::getData()
	 */
	public function getData() : Iterator
	{
		return $this->_data ?? new ArrayIterator();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \IteratorAggregate::getIterator()
	 * @return Iterator<integer, T>
	 */
	public function getIterator() : Iterator
	{
		return $this->getData();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Countable::count()
	 */
	public function count() : int
	{
		return \count(\iterator_to_array($this->getData()));
	}
	
}
