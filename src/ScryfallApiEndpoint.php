<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use InvalidArgumentException;
use PhpExtended\Endpoint\HttpJsonEndpoint;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Uuid\UuidInterface;
use Stringable;

/**
 * ScryfallApiEndpoint class file.
 *
 * This class is the entry point for all methods using this api.
 *
 * @author Anastaszor
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ScryfallApiEndpoint extends HttpJsonEndpoint implements ScryfallApiEndpointInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Endpoint\HttpEndpoint::postConstruct()
	 * @throws InvalidArgumentException
	 */
	protected function postConstruct() : void
	{
		$this->http()->setDefaultHostname('api.scryfall.com');
		
		$this->getConfiguration()->setImplementation(ScryfallApiBulkInterface::class, ScryfallApiBulk::class);
		$this->getConfiguration()->setImplementation(ScryfallApiCardInterface::class, ScryfallApiCard::class);
		$this->getConfiguration()->setImplementation(ScryfallApiCardFaceInterface::class, ScryfallApiCardFace::class);
		$this->getConfiguration()->setImplementation(ScryfallApiCardImageryInterface::class, ScryfallApiCardImagery::class);
		$this->getConfiguration()->setImplementation(ScryfallApiCardLegalityInterface::class, ScryfallApiCardLegality::class);
		$this->getConfiguration()->setImplementation(ScryfallApiCardPurchaseInterface::class, ScryfallApiCardPurchase::class);
		$this->getConfiguration()->setImplementation(ScryfallApiCardResourceInterface::class, ScryfallApiCardResource::class);
		$this->getConfiguration()->setImplementation(ScryfallApiCatalogInterface::class, ScryfallApiCatalog::class);
		$this->getConfiguration()->setImplementation(ScryfallApiErrorInterface::class, ScryfallApiError::class);
		$this->getConfiguration()->setImplementation(ScryfallApiExtensionInterface::class, ScryfallApiExtension::class);
		$this->getConfiguration()->setImplementation(ScryfallApiPaginationInterface::class, ScryfallApiPagination::class);
		$this->getConfiguration()->setImplementation(ScryfallApiParsedManaInterface::class, ScryfallApiParsedMana::class);
		$this->getConfiguration()->setImplementation(ScryfallApiPreviewInterface::class, ScryfallApiPreview::class);
		$this->getConfiguration()->setImplementation(ScryfallApiPricesInterface::class, ScryfallApiPrices::class);
		$this->getConfiguration()->setImplementation(ScryfallApiRelatedCardInterface::class, ScryfallApiRelatedCard::class);
		$this->getConfiguration()->setImplementation(ScryfallApiRulingInterface::class, ScryfallApiRuling::class);
		$this->getConfiguration()->setImplementation(ScryfallApiSymbolInterface::class, ScryfallApiSymbol::class);
		
		$this->getConfiguration()->addIgnoreExcessFields(ScryfallApiPagination::class, ['object']);
		$this->getConfiguration()->addIgnoreExcessFields(ScryfallApiExtension::class, ['object']);
		$this->getConfiguration()->addIgnoreExcessFields(ScryfallApiCatalog::class, ['object']);
		$this->getConfiguration()->addIgnoreExcessFields(ScryfallApiCard::class, ['object']);
		$this->getConfiguration()->addIgnoreExcessFields(ScryfallApiRuling::class, ['object']);
		$this->getConfiguration()->addIgnoreExcessFields(ScryfallApiParsedMana::class, ['object']);
		$this->getConfiguration()->addIgnoreExcessFields(ScryfallApiRelatedCard::class, ['object']);
		$this->getConfiguration()->addIgnoreExcessFields(ScryfallApiCardFace::class, ['object']);
		
		$this->getConfiguration()->setIterableInnerTypes(ScryfallApiPagination::class, ['data'], Stringable::class);
		$this->getConfiguration()->setPolymorphismKey(Stringable::class, 'object');
		$this->getConfiguration()->setPolymorphismClass(Stringable::class, 'set', ScryfallApiExtension::class);
		$this->getConfiguration()->setPolymorphismClass(Stringable::class, 'card_symbol', ScryfallApiSymbol::class);
		$this->getConfiguration()->setPolymorphismClass(Stringable::class, 'ruling', ScryfallApiRuling::class);
		$this->getConfiguration()->setPolymorphismClass(Stringable::class, 'card', ScryfallApiCard::class);
		
		$this->getConfiguration()->setIterableInnerTypes(ScryfallApiCard::class, ['multiverse_ids'], 'int');
		$this->getConfiguration()->setIterableInnerTypes(ScryfallApiCard::class, ['all_parts'], ScryfallApiRelatedCard::class);
		$this->getConfiguration()->setIterableInnerTypes(ScryfallApiCard::class, ['card_faces'], ScryfallApiCardFace::class);
		$this->getConfiguration()->setIterableInnerTypes(ScryfallApiCard::class, ['colors', 'color_identity', 'color_indicator', 'artist_ids', 'finishes', 'frame_effects', 'games', 'produced_mana', 'promo_types', 'keywords'], 'string');
		$this->getConfiguration()->setIterableInnerTypes(ScryfallApiCardFace::class, ['color_indicator', 'colors', 'image_uris'], 'string');
		$this->getConfiguration()->setIterableInnerTypes(ScryfallApiCatalog::class, ['data'], 'string');
		$this->getConfiguration()->setIterableInnerTypes(ScryfallApiParsedMana::class, ['colors'], 'string');
		$this->getConfiguration()->setIterableInnerTypes(ScryfallApiSymbol::class, ['colors', 'gatherer_alternates'], 'string');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getSets()
	 * @throws InvalidArgumentException should not happen
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getSets() : ScryfallApiPaginationInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->jsonObjectGet(ScryfallApiPagination::class, '/sets');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getSetByCode()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getSetByCode(string $setCode) : ScryfallApiExtensionInterface
	{
		return $this->jsonObjectGet(ScryfallApiExtension::class, '/sets/'.\urlencode($setCode));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getSetByTcgplayerId()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getSetByTcgplayerId(int $groupId) : ScryfallApiExtensionInterface
	{
		return $this->jsonObjectGet(ScryfallApiExtension::class, '/sets/tcgplayer/'.\urlencode((string) $groupId));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getSetByScryfallId()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getSetByScryfallId(UuidInterface $setId) : ScryfallApiExtensionInterface
	{
		return $this->jsonObjectGet(ScryfallApiExtension::class, '/sets/'.$setId->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardsSearch()
	 * @throws InvalidArgumentException should not happen
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getCardsSearch(int $page = 1, ?string $query = null, ?string $unique = null, ?string $order = null, ?string $dir = null, bool $includeExtras = false, bool $includeMultilingual = false, bool $includeVariations = false) : ScryfallApiPaginationInterface
	{
		$url = '/cards/search?page='.((string) $page).'&q='.\urlencode((string) $query);
		if(null !== $unique)
			$url .= '&unique='.\urlencode($unique);
		if(null !== $order)
			$url .= '&order='.\urlencode($order);
		if(null !== $dir)
			$url .= '&dir='.\urlencode($dir);
		if($includeExtras)
			$url .= '&include_extras';
		if($includeMultilingual)
			$url .= '&include_multilingual';
		if($includeVariations)
			$url .= '&include_variations';
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->jsonObjectGet(ScryfallApiPagination::class, $url);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardNamed()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardNamed(string $name, bool $fuzzy = false, ?string $set = null) : ?ScryfallApiCardInterface
	{
		$url = '/cards/named?';
		$url .= ($fuzzy ? 'fuzzy' : 'exact').'='.\urlencode($name);
		if(null !== $set)
			$url .= '&set='.\urlencode($set);
		
		return $this->jsonObjectGet(ScryfallApiCard::class, $url);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardsAutocomplete()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardsAutocomplete(string $query, bool $includeExtras = false) : ScryfallApiCatalogInterface
	{
		$url = '/cards/autocomplete?q='.\urlencode($query);
		if($includeExtras)
			$url .= '&include_extras';
		
		return $this->jsonObjectGet(ScryfallApiCatalog::class, $url);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardRandom()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardRandom(?string $query = null) : ScryfallApiCardInterface
	{
		$url = '/cards/random';
		if(null !== $query)
			$url .= '?q='.\urlencode($query);
		
		return $this->jsonObjectGet(ScryfallApiCard::class, $url);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCard()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCard(string $setCode, string $collNb, ?string $lang = null) : ScryfallApiCardInterface
	{
		$url = '/cards/'.\urlencode($setCode).'/'.\urlencode($collNb);
		if(null !== $lang)
			$url .= '/'.\urlencode($lang);
		
		return $this->jsonObjectGet(ScryfallApiCard::class, $url);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardByMultiverseId()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardByMultiverseId(int $multiverseId) : ScryfallApiCardInterface
	{
		return $this->jsonObjectGet(ScryfallApiCard::class, '/cards/multiverse/'.((string) $multiverseId));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardByMtgoId()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardByMtgoId(int $mtgoId) : ScryfallApiCardInterface
	{
		return $this->jsonObjectGet(ScryfallApiCard::class, '/cards/mtgo/'.((string) $mtgoId));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardByArenaId()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardByArenaId(int $arenaId) : ScryfallApiCardInterface
	{
		return $this->jsonObjectGet(ScryfallApiCard::class, '/cards/arena/'.((string) $arenaId));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardByTcgplayerId()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardByTcgplayerId(int $tcgpId) : ScryfallApiCardInterface
	{
		return $this->jsonObjectGet(ScryfallApiCard::class, '/cards/tcgplayer/'.((string) $tcgpId));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardByCardmarketId()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardByCardmarketId(int $mcmId) : ScryfallApiCardInterface
	{
		return $this->jsonObjectGet(ScryfallApiCard::class, '/cards/cardmarket/'.((string) $mcmId));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardByScryfallId()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardByScryfallId(UuidInterface $uuid) : ScryfallApiCardInterface
	{
		return $this->jsonObjectGet(ScryfallApiCard::class, '/cards/'.$uuid->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardImage()
	 * @throws InvalidArgumentException should not happen
	 * @throws ParseThrowable
	 */
	public function getCardImage(UuidInterface $cardId, string $format = 'large', bool $back = false) : string
	{
		$url = '/cards/'.$cardId->__toString().'?format=image&version='.\urlencode($format);
		if($back)
			$url .= '&face=back';
		
		return $this->http()->httpGet($url)->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getRulingsFromMultiverseId()
	 * @throws InvalidArgumentException should not happen
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromMultiverseId(int $multiverseId) : ScryfallApiPaginationInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->jsonObjectGet(ScryfallApiPagination::class, '/cards/multiverse/'.((string) $multiverseId).'/rulings');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getRulingsFromMtgoId()
	 * @throws InvalidArgumentException should not happen
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromMtgoId(int $mtgoId) : ScryfallApiPaginationInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->jsonObjectGet(ScryfallApiPagination::class, '/cards/mtgo/'.((string) $mtgoId).'/rulings');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getRulingsFromArenaId()
	 * @throws InvalidArgumentException should not happen
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromArenaId(int $arenaId) : ScryfallApiPaginationInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->jsonObjectGet(ScryfallApiPagination::class, '/cards/arena/'.((string) $arenaId).'/rulings');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getRulingsFromCard()
	 * @throws InvalidArgumentException should not happen
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromCard(string $extCode, string $cardNb) : ScryfallApiPaginationInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->jsonObjectGet(ScryfallApiPagination::class, '/cards/'.\urlencode($extCode).'/'.\urlencode($cardNb).'/rulings');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getRulingsFromScryfallId()
	 * @throws InvalidArgumentException should not happen
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getRulingsFromScryfallId(UuidInterface $scryfallCardId) : ScryfallApiPaginationInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->jsonObjectGet(ScryfallApiPagination::class, '/cards/'.$scryfallCardId->__toString().'/rulings');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getSymbology()
	 * @throws InvalidArgumentException should not happen
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getSymbology() : ScryfallApiPaginationInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return $this->jsonObjectGet(ScryfallApiPagination::class, '/symbology');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getParsedMana()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getParsedMana(string $cost) : ScryfallApiParsedManaInterface
	{
		return $this->jsonObjectGet(ScryfallApiParsedMana::class, '/symbology/parse-mana?cost='.\urlencode($cost));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCardNames()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCardNames() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/card-names');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getArtistNames()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getArtistNames() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/artist-names');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getWordBank()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getWordBank() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/word-bank');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getCreatureTypes()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getCreatureTypes() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/creature-types');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getPlaneswalkerTypes()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getPlaneswalkerTypes() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/planeswalker-types');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getLandTypes()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getLandTypes() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/land-types');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getArtifactTypes()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getArtifactTypes() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/artifact-types');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getEnchantmentTypes()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getEnchantmentTypes() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/enchantment-types');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getSpellTypes()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getSpellTypes() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/spell-types');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getPowers()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getPowers() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/powers');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getToughnesses()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getToughnesses() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/toughnesses');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getLoyalties()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getLoyalties() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/loyalties');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiEndpointInterface::getWatermarks()
	 * @throws InvalidArgumentException should not happen
	 */
	public function getWatermarks() : ScryfallApiCatalogInterface
	{
		return $this->jsonObjectGet(ScryfallApiCatalog::class, '/catalog/watermarks');
	}
	
}
