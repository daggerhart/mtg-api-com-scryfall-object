<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiCardPurchase class file.
 * 
 * This class represents third party related website urls for this card.
 * 
 * @author Anastaszor
 */
class ScryfallApiCardPurchase implements ScryfallApiCardPurchaseInterface
{
	
	/**
	 * The url to the tcgplayer website page for this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_tcgplayer;
	
	/**
	 * The url to the cardmarket website page for this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_cardmarket;
	
	/**
	 * The url to the cardhoarder website page for this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_cardhoarder;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL CARD PURCHASE';
	}
	
	/**
	 * Sets the uri to tcgplayer.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardPurchase
	 */
	public function setTcgplayer(?UriInterface $uri) : ScryfallApiCardPurchase
	{
		$this->_tcgplayer = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardPurchaseInterface::getTcgplayer()
	 */
	public function getTcgplayer() : ?UriInterface
	{
		return $this->_tcgplayer;
	}
	
	/**
	 * Sets the uri to cardmarket.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardPurchase
	 */
	public function setCardmarket(?UriInterface $uri) : ScryfallApiCardPurchase
	{
		$this->_cardmarket = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardPurchaseInterface::getCardmarket()
	 */
	public function getCardmarket() : ?UriInterface
	{
		return $this->_cardmarket;
	}
	
	/**
	 * Sets the uri to cardhoarder.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardPurchase
	 */
	public function setCardhoarder(?UriInterface $uri) : ScryfallApiCardPurchase
	{
		$this->_cardhoarder = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardPurchaseInterface::getCardhoarder()
	 */
	public function getCardhoarder() : ?UriInterface
	{
		return $this->_cardhoarder;
	}
	
}
