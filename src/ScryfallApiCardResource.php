<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiCardResource class file.
 * 
 * This class represents other resources for the card.
 * 
 * @author Anastaszor
 */
class ScryfallApiCardResource implements ScryfallApiCardResourceInterface
{
	
	/**
	 * The url to the gatherer page of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_gatherer;
	
	/**
	 * The url to the tcgplayer decks page of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_tcgplayerDecks;
	
	/**
	 * The url to the tcgplayer article page of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_tcgplayerInfiniteArticles;
	
	/**
	 * The url to the tcgplayer decks page of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_tcgplayerInfiniteDecks;
	
	/**
	 * The url to the edhrec page of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_edhrec;
	
	/**
	 * The url to the mtgtop8 page of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_mtgtop8;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL CARD RESOURCE';
	}
	
	/**
	 * Sets the uri to gatherer.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardResource
	 */
	public function setGatherer(?UriInterface $uri) : ScryfallApiCardResource
	{
		$this->_gatherer = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardResourceInterface::getGatherer()
	 */
	public function getGatherer() : ?UriInterface
	{
		return $this->_gatherer;
	}
	
	/**
	 * Sets the uri to tcgplayer decks.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardResource
	 */
	public function setTcgplayerDecks(?UriInterface $uri) : ScryfallApiCardResource
	{
		$this->_tcgplayerDecks = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardResourceInterface::getTcgplayerDecks()
	 */
	public function getTcgplayerDecks() : ?UriInterface
	{
		return $this->_tcgplayerDecks;
	}
	
	/**
	 * Sets the uri to tcgplayer infinite articles.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardResource
	 */
	public function setTcgplayerInfiniteArticles(?UriInterface $uri) : ScryfallApiCardResource
	{
		$this->_tcgplayerInfiniteArticles = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardResourceInterface::getTcgplayerInfiniteArticles()
	 */
	public function getTcgplayerInfiniteArticles() : ?UriInterface
	{
		return $this->_tcgplayerInfiniteArticles;
	}
	
	/**
	 * Sets the uri to tcgplyer infinite decks.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardResource
	 */
	public function setTcgplayerInfiniteDecks(?UriInterface $uri) : ScryfallApiCardResource
	{
		$this->_tcgplayerInfiniteDecks = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardResourceInterface::getTcgplayerInfiniteDecks()
	 */
	public function getTcgplayerInfiniteDecks() : ?UriInterface
	{
		return $this->_tcgplayerInfiniteDecks;
	}
	
	/**
	 * Sets the uri to edhrec.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardResource
	 */
	public function setEdhrec(?UriInterface $uri) : ScryfallApiCardResource
	{
		$this->_edhrec = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardResourceInterface::getEdhrec()
	 */
	public function getEdhrec() : ?UriInterface
	{
		return $this->_edhrec;
	}
	
	/**
	 * Sets the uri to mtgtop8.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardResource
	 */
	public function setMtgtop8(?UriInterface $uri) : ScryfallApiCardResource
	{
		$this->_mtgtop8 = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardResourceInterface::getMtgtop8()
	 */
	public function getMtgtop8() : ?UriInterface
	{
		return $this->_mtgtop8;
	}
	
}
