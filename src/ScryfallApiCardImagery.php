<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiCardImagery class file.
 * 
 * This class represents the image links of a given card.
 * 
 * @author Anastaszor
 */
class ScryfallApiCardImagery implements ScryfallApiCardImageryInterface
{
	
	/**
	 * The url to the small image of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_small;
	
	/**
	 * The url to the normal image of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_normal;
	
	/**
	 * The url to the large image of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_large;
	
	/**
	 * The url to the png image of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_png;
	
	/**
	 * The url to the art crop image of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_artCrop;
	
	/**
	 * The url to the border crop image of this card.
	 * 
	 * @var ?UriInterface
	 */
	protected $_borderCrop;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL CARD IMAGERY';
	}
	
	/**
	 * Sets the uri to the small image.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardImagery
	 */
	public function setSmall(?UriInterface $uri) : ScryfallApiCardImagery
	{
		$this->_small = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardImageryInterface::getSmall()
	 */
	public function getSmall() : ?UriInterface
	{
		return $this->_small;
	}
	
	/**
	 * Sets the uri to the normal image.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardImagery
	 */
	public function setNormal(?UriInterface $uri) : ScryfallApiCardImagery
	{
		$this->_normal = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardImageryInterface::getNormal()
	 */
	public function getNormal() : ?UriInterface
	{
		return $this->_normal;
	}
	
	/**
	 * Sets the uri to the large image.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardImagery
	 */
	public function setLarge(?UriInterface $uri) : ScryfallApiCardImagery
	{
		$this->_large = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardImageryInterface::getLarge()
	 */
	public function getLarge() : ?UriInterface
	{
		return $this->_large;
	}
	
	/**
	 * Sets the uri to the png image.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardImagery
	 */
	public function setPng(?UriInterface $uri) : ScryfallApiCardImagery
	{
		$this->_png = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardImageryInterface::getPng()
	 */
	public function getPng() : ?UriInterface
	{
		return $this->_png;
	}
	
	/**
	 * Sets the uri to the art crop image.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardImagery
	 */
	public function setArtCrop(?UriInterface $uri) : ScryfallApiCardImagery
	{
		$this->_artCrop = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardImageryInterface::getArtCrop()
	 */
	public function getArtCrop() : ?UriInterface
	{
		return $this->_artCrop;
	}
	
	/**
	 * Sets the uri to the border crop image.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCardImagery
	 */
	public function setBorderCrop(?UriInterface $uri) : ScryfallApiCardImagery
	{
		$this->_borderCrop = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardImageryInterface::getBorderCrop()
	 */
	public function getBorderCrop() : ?UriInterface
	{
		return $this->_borderCrop;
	}
	
}
