<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiSymbol class file.
 * 
 * This class represents a symbol.
 * 
 * @author Anastaszor
 */
class ScryfallApiSymbol implements ScryfallApiSymbolInterface
{
	
	/**
	 * The plaintext symbol. Often surrounded with curly braces {}. Note that
	 * not all symbols are ASCII text (for example, {∞}).
	 * 
	 * @var string
	 */
	protected $_symbol;
	
	/**
	 * An alternate version of this symbol, if it is possible to write it
	 * without curly braces.
	 * 
	 * @var ?string
	 */
	protected $_looseVariant;
	
	/**
	 * An English snippet that describes this symbol. Appropriate for use in
	 * alt text or other accessible communication formats.
	 * 
	 * @var ?string
	 */
	protected $_english;
	
	/**
	 * True if it is possible to write this symbol “backwards”. For example,
	 * the official symbol {U/P} is sometimes written as {P/U} or {P\U} in
	 * informal settings. Note that the Scryfall API never writes symbols
	 * backwards in other responses. This field is provided for informational
	 * purposes.
	 * 
	 * @var ?boolean
	 */
	protected $_transposable;
	
	/**
	 * True if this is a mana symbol.
	 * 
	 * @var ?boolean
	 */
	protected $_representsMana;
	
	/**
	 * A decimal number representing this symbol’s converted mana cost. Note
	 * that mana symbols from funny sets can have fractional converted mana
	 * costs.
	 * 
	 * @var ?float
	 */
	protected $_cmc;
	
	/**
	 * True if this symbol appears in a mana cost on any Magic card. For
	 * example {20} has this field set to false because {20} only appears in
	 * Oracle text, not mana costs.
	 * 
	 * @var ?boolean
	 */
	protected $_appearsInManaCosts;
	
	/**
	 * True if this symbol is only used on funny cards or Un-cards.
	 * 
	 * @var ?boolean
	 */
	protected $_funny;
	
	/**
	 * An array of colors that this symbol represents.
	 * 
	 * @var array<integer, string>
	 */
	protected $_colors = [];
	
	/**
	 * An array of plaintext versions of this symbol that Gatherer uses on old
	 * cards to describe original printed text. For example: {W} has ["oW",
	 * "ooW"] as alternates.
	 * 
	 * @var array<integer, string>
	 */
	protected $_gathererAlternates = [];
	
	/**
	 * The uri for the svg file.
	 * 
	 * @var ?UriInterface
	 */
	protected $_svgUri;
	
	/**
	 * Builds a new ScryfallApiSymbol with its data.
	 * 
	 * @param string $symbol
	 */
	public function __construct(string $symbol)
	{
		$this->_symbol = $symbol;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL SYMBOL '.$this->getSymbol();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::getSymbol()
	 */
	public function getSymbol() : string
	{
		return $this->_symbol;
	}
	
	/**
	 * Sets the loose variant of this symbol.
	 * 
	 * @param ?string $looseVariant
	 * @return ScryfallApiSymbol
	 */
	public function setLooseVariant(?string $looseVariant) : ScryfallApiSymbol
	{
		$this->_looseVariant = $looseVariant;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::getLooseVariant()
	 */
	public function getLooseVariant() : ?string
	{
		return $this->_looseVariant;
	}
	
	/**
	 * Sets the english name of the symbol.
	 * 
	 * @param ?string $english
	 * @return ScryfallApiSymbol
	 */
	public function setEnglish(?string $english) : ScryfallApiSymbol
	{
		$this->_english = $english;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::getEnglish()
	 */
	public function getEnglish() : ?string
	{
		return $this->_english;
	}
	
	/**
	 * Sets whether this symbol is transposable.
	 * 
	 * @param ?boolean $transposable
	 * @return ScryfallApiSymbol
	 */
	public function setTransposable(?bool $transposable) : ScryfallApiSymbol
	{
		$this->_transposable = $transposable;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::isTransposable()
	 */
	public function isTransposable() : ?bool
	{
		return $this->_transposable;
	}
	
	/**
	 * Sets whether this symbol represents mana.
	 * 
	 * @param ?boolean $representsMana
	 * @return ScryfallApiSymbol
	 */
	public function setRepresentsMana(?bool $representsMana) : ScryfallApiSymbol
	{
		$this->_representsMana = $representsMana;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::isRepresentsMana()
	 */
	public function isRepresentsMana() : ?bool
	{
		return $this->_representsMana;
	}
	
	/**
	 * Sets the cmc of the symbol.
	 * 
	 * @param ?float $cmc
	 * @return ScryfallApiSymbol
	 */
	public function setCmc(?float $cmc) : ScryfallApiSymbol
	{
		$this->_cmc = $cmc;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::getCmc()
	 */
	public function getCmc() : ?float
	{
		return $this->_cmc;
	}
	
	/**
	 * Sets whether this symbol appears in mana costs.
	 * 
	 * @param ?boolean $appears
	 * @return ScryfallApiSymbol
	 */
	public function setAppearsInManaCosts(?bool $appears) : ScryfallApiSymbol
	{
		$this->_appearsInManaCosts = $appears;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::isAppearsInManaCosts()
	 */
	public function isAppearsInManaCosts() : ?bool
	{
		return $this->_appearsInManaCosts;
	}
	
	/**
	 * Sets whether this symbol is funny.
	 * 
	 * @param ?boolean $isFunny
	 * @return ScryfallApiSymbol
	 */
	public function setFunny(?bool $isFunny) : ScryfallApiSymbol
	{
		$this->_funny = $isFunny;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::isFunny()
	 */
	public function isFunny() : ?bool
	{
		return $this->_funny;
	}
	
	/**
	 * Sets the colors.
	 * 
	 * @param array<integer, string> $colors
	 * @return ScryfallApiSymbol
	 */
	public function setColors(array $colors) : ScryfallApiSymbol
	{
		$this->_colors = $colors;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::getColors()
	 */
	public function getColors() : array
	{
		return $this->_colors;
	}
	
	/**
	 * Sets the gatherer alternates.
	 * 
	 * @param array<integer, string> $gathererAlternates
	 * @return ScryfallApiSymbol
	 */
	public function setGathererAlternates(array $gathererAlternates) : ScryfallApiSymbol
	{
		$this->_gathererAlternates = $gathererAlternates;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::getGathererAlternates()
	 */
	public function getGathererAlternates() : array
	{
		return $this->_gathererAlternates;
	}
	
	/**
	 * Sets the svg uri.
	 * 
	 * @param UriInterface $svgUri
	 * @return ScryfallApiSymbol
	 */
	public function setSvgUri(?UriInterface $svgUri) : ScryfallApiSymbol
	{
		$this->_svgUri = $svgUri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiSymbolInterface::getSvgUri()
	 */
	public function getSvgUri() : ?UriInterface
	{
		return $this->_svgUri;
	}
	
}
