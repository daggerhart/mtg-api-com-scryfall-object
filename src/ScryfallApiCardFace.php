<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use PhpExtended\Uuid\UuidInterface;

/**
 * ScryfallApiCardFace class file.
 * 
 * This class represents a face of a given card from the scryfall api.
 * 
 * @author Anastaszor
 * @SuppressWarnings(PHPMD.TooManyFields)
 */
class ScryfallApiCardFace implements ScryfallApiCardFaceInterface
{
	
	/**
	 * The id of the illustrator of this card face.
	 * 
	 * @var ?string
	 */
	protected $_artistId;
	
	/**
	 * The name of the illustrator of this card face. Newly spoiled cards may
	 * not have this field yet.
	 * 
	 * @var ?string
	 */
	protected $_artist;
	
	/**
	 * The colors in this face’s color indicator, if any.
	 * 
	 * @var array<integer, string>
	 */
	protected $_colorIndicator = [];
	
	/**
	 * This face’s colors, if the game defines colors for the individual face
	 * of this card.
	 * 
	 * @var array<integer, string>
	 */
	protected $_colors = [];
	
	/**
	 * The flavor name printed on this face, if any.
	 * 
	 * @var ?string
	 */
	protected $_flavorName;
	
	/**
	 * The flavor text printed on this face, if any.
	 * 
	 * @var ?string
	 */
	protected $_flavorText;
	
	/**
	 * A unique identifier for the card face artwork that remains consistent
	 * across reprints. Newly spoiled cards may not have this field yet.
	 * 
	 * @var ?UuidInterface
	 */
	protected $_illustrationId;
	
	/**
	 * An object providing URIs to imagery for this face, if this is a
	 * double-sided card. If this card is not double-sided, then the image_uris
	 * property will be part of the parent object instead.
	 * 
	 * @var array<integer, string>
	 */
	protected $_imageUris = [];
	
	/**
	 * This face’s loyalty, if any.
	 * 
	 * @var ?string
	 */
	protected $_loyalty;
	
	/**
	 * The mana cost for this face. This value will be any empty string "" if
	 * the cost is absent. Remember that per the game rules, a missing mana
	 * cost and a mana cost of {0} are different values.
	 * 
	 * @var ?string
	 */
	protected $_manaCost;
	
	/**
	 * The name of this particular face.
	 * 
	 * @var ?string
	 */
	protected $_name;
	
	/**
	 * The Oracle text for this face, if any.
	 * 
	 * @var ?string
	 */
	protected $_oracleText;
	
	/**
	 * This face’s power, if any. Note that some cards have powers that are not
	 * numeric, such as *.
	 * 
	 * @var ?string
	 */
	protected $_power;
	
	/**
	 * The localized name printed on this face, if any.
	 * 
	 * @var ?string
	 */
	protected $_printedName;
	
	/**
	 * The localized text printed on this face, if any.
	 * 
	 * @var ?string
	 */
	protected $_printedText;
	
	/**
	 * The localized type line printed on this face, if any.
	 * 
	 * @var ?string
	 */
	protected $_printedTypeLine;
	
	/**
	 * This face’s toughness, if any.
	 * 
	 * @var ?string
	 */
	protected $_toughness;
	
	/**
	 * The type line of this particular face.
	 * 
	 * @var ?string
	 */
	protected $_typeLine;
	
	/**
	 * The watermark on this particulary card face, if any.
	 * 
	 * @var ?string
	 */
	protected $_watermark;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL CARD FACE';
	}
	
	/**
	 * Sets the artist id of this card face.
	 * 
	 * @param ?string $artist
	 * @return ScryfallApiCardFace
	 */
	public function setArtistId(?string $artist) : ScryfallApiCardFace
	{
		$this->_artistId = $artist;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getArtistId()
	 */
	public function getArtistId() : ?string
	{
		return $this->_artistId;
	}
	
	/**
	 * Sets the artist of this card face.
	 * 
	 * @param ?string $artist
	 * @return ScryfallApiCardFace
	 */
	public function setArtist(?string $artist) : ScryfallApiCardFace
	{
		$this->_artist = $artist;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getArtist()
	 */
	public function getArtist() : ?string
	{
		return $this->_artist;
	}
	
	/**
	 * Sets the colors of the indicator of this card face.
	 * 
	 * @param array<integer, string> $colors
	 * @return ScryfallApiCardFace
	 */
	public function setColorIndicator(array $colors) : ScryfallApiCardFace
	{
		$this->_colorIndicator = $colors;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getColorIndicator()
	 */
	public function getColorIndicator() : array
	{
		return $this->_colorIndicator;
	}
	
	/**
	 * Sets the colors of this card face.
	 * 
	 * @param array<integer, string> $colors
	 * @return ScryfallApiCardFace
	 */
	public function setColors(array $colors) : ScryfallApiCardFace
	{
		$this->_colors = $colors;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getColors()
	 */
	public function getColors() : array
	{
		return $this->_colors;
	}
	
	/**
	 * Sets the flavor name of this card face.
	 * 
	 * @param ?string $flavorName
	 * @return ScryfallApiCardFace
	 */
	public function setFlavorName(?string $flavorName) : ScryfallApiCardFace
	{
		$this->_flavorName = $flavorName;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getFlavorName()
	 */
	public function getFlavorName() : ?string
	{
		return $this->_flavorName;
	}
	
	/**
	 * Sets the flavor text of this card face.
	 * 
	 * @param string $flavorText
	 * @return ScryfallApiCardFace
	 */
	public function setFlavorText(?string $flavorText) : ScryfallApiCardFace
	{
		$this->_flavorText = $flavorText;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getFlavorText()
	 */
	public function getFlavorText() : ?string
	{
		return $this->_flavorText;
	}
	
	/**
	 * Sets the illustration id of this card face.
	 * 
	 * @param ?UuidInterface $uuid
	 * @return ScryfallApiCardFace
	 */
	public function setIllustrationId(?UuidInterface $uuid) : ScryfallApiCardFace
	{
		$this->_illustrationId = $uuid;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getIllustrationId()
	 */
	public function getIllustrationId() : ?UuidInterface
	{
		return $this->_illustrationId;
	}
	
	/**
	 * Sets the image uris of this card face.
	 * 
	 * @param array<integer, string> $uris
	 * @return ScryfallApiCardFace
	 */
	public function setImageUris(array $uris) : ScryfallApiCardFace
	{
		$this->_imageUris = $uris;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getImageUris()
	 */
	public function getImageUris() : array
	{
		return $this->_imageUris;
	}
	
	/**
	 * Sets the loyalty of this card face.
	 * 
	 * @param ?string $loyalty
	 * @return ScryfallApiCardFace
	 */
	public function setLoyalty(?string $loyalty) : ScryfallApiCardFace
	{
		$this->_loyalty = $loyalty;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getLoyalty()
	 */
	public function getLoyalty() : ?string
	{
		return $this->_loyalty;
	}
	
	/**
	 * Sets the mana cost of this card face.
	 * 
	 * @param ?string $manacost
	 * @return ScryfallApiCardFace
	 */
	public function setManaCost(?string $manacost) : ScryfallApiCardFace
	{
		$this->_manaCost = $manacost;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getManaCost()
	 */
	public function getManaCost() : ?string
	{
		return $this->_manaCost;
	}
	
	/**
	 * Sets the name of this card face.
	 * 
	 * @param ?string $name
	 * @return ScryfallApiCardFace
	 */
	public function setName(?string $name) : ScryfallApiCardFace
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getName()
	 */
	public function getName() : ?string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the oracle text of this card face.
	 * 
	 * @param ?string $oracleText
	 * @return ScryfallApiCardFace
	 */
	public function setOracleText(?string $oracleText) : ScryfallApiCardFace
	{
		$this->_oracleText = $oracleText;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getOracleText()
	 */
	public function getOracleText() : ?string
	{
		return $this->_oracleText;
	}
	
	/**
	 * Sets the power of this card face.
	 * 
	 * @param ?string $power
	 * @return ScryfallApiCardFace
	 */
	public function setPower(?string $power) : ScryfallApiCardFace
	{
		$this->_power = $power;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getPower()
	 */
	public function getPower() : ?string
	{
		return $this->_power;
	}
	
	/**
	 * Sets the printed name of this card face.
	 * 
	 * @param ?string $printedName
	 * @return ScryfallApiCardFace
	 */
	public function setPrintedName(?string $printedName) : ScryfallApiCardFace
	{
		$this->_printedName = $printedName;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getPrintedName()
	 */
	public function getPrintedName() : ?string
	{
		return $this->_printedName;
	}
	
	/**
	 * Sets the printed text of this card face.
	 * 
	 * @param ?string $printedText
	 * @return ScryfallApiCardFace
	 */
	public function setPrintedText(?string $printedText) : ScryfallApiCardFace
	{
		$this->_printedText = $printedText;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getPrintedText()
	 */
	public function getPrintedText() : ?string
	{
		return $this->_printedText;
	}
	
	/**
	 * Sets the printed type line.
	 * 
	 * @param ?string $typeLine
	 * @return ScryfallApiCardFace
	 */
	public function setPrintedTypeLine(?string $typeLine) : ScryfallApiCardFace
	{
		$this->_printedTypeLine = $typeLine;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getPrintedTypeLine()
	 */
	public function getPrintedTypeLine() : ?string
	{
		return $this->_printedTypeLine;
	}
	
	/**
	 * Sets the toughness of this card face.
	 * 
	 * @param ?string $toughness
	 * @return ScryfallApiCardFace
	 */
	public function setToughness(?string $toughness) : ScryfallApiCardFace
	{
		$this->_toughness = $toughness;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getToughness()
	 */
	public function getToughness() : ?string
	{
		return $this->_toughness;
	}
	
	/**
	 * Sets the type line of this card face.
	 * 
	 * @param ?string $typeLine
	 * @return ScryfallApiCardFace
	 */
	public function setTypeLine(?string $typeLine) : ScryfallApiCardFace
	{
		$this->_typeLine = $typeLine;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getTypeLine()
	 */
	public function getTypeLine() : ?string
	{
		return $this->_typeLine;
	}
	
	/**
	 * Sets the watermark of the card face.
	 * 
	 * @param ?string $watermark
	 * @return ScryfallApiCardFace
	 */
	public function setWatermark(?string $watermark) : ScryfallApiCardFace
	{
		$this->_watermark = $watermark;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCardFaceInterface::getWatermark()
	 */
	public function getWatermark() : ?string
	{
		return $this->_watermark;
	}
	
}
