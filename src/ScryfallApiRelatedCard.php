<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiRelatedCard class file.
 * 
 * This class represents the related card links to this card.
 * 
 * @author Anastaszor
 */
class ScryfallApiRelatedCard implements ScryfallApiRelatedCardInterface
{
	
	/**
	 * An unique ID for this card in Scryfall’s database.
	 * 
	 * @var UuidInterface
	 */
	protected $_id;
	
	/**
	 * A field explaining what role this card plays in this relationship, one
	 * of token, meld_part, meld_result, or combo_piece.
	 * 
	 * @var ?string
	 */
	protected $_component;
	
	/**
	 * The name of this particular related card.
	 * 
	 * @var ?string
	 */
	protected $_name;
	
	/**
	 * The type line of this card.
	 * 
	 * @var ?string
	 */
	protected $_typeLine;
	
	/**
	 * A URI where you can retrieve a full object describing this card on
	 * Scryfall’s API.
	 * 
	 * @var ?UriInterface
	 */
	protected $_uri;
	
	/**
	 * Builds a new ScryfallApiRelatedCard with its data.
	 * 
	 * @param UuidInterface $id
	 */
	public function __construct(UuidInterface $id)
	{
		$this->_id = $id;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL RELATED CARD '.$this->_id->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiRelatedCardInterface::getId()
	 */
	public function getId() : UuidInterface
	{
		return $this->_id;
	}
	
	/**
	 * Sets the component of the related.
	 * 
	 * @param ?string $component
	 * @return ScryfallApiRelatedCard
	 */
	public function setComponent(?string $component) : ScryfallApiRelatedCard
	{
		$this->_component = $component;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiRelatedCardInterface::getComponent()
	 */
	public function getComponent() : ?string
	{
		return $this->_component;
	}
	
	/**
	 * Sets the name of the related.
	 * 
	 * @param ?string $name
	 * @return ScryfallApiRelatedCard
	 */
	public function setName(?string $name) : ScryfallApiRelatedCard
	{
		$this->_name = $name;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiRelatedCardInterface::getName()
	 */
	public function getName() : ?string
	{
		return $this->_name;
	}
	
	/**
	 * Sets the typeline of the related.
	 * 
	 * @param ?string $typeLine
	 * @return ScryfallApiRelatedCard
	 */
	public function setTypeLine(?string $typeLine) : ScryfallApiRelatedCard
	{
		$this->_typeLine = $typeLine;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiRelatedCardInterface::getTypeLine()
	 */
	public function getTypeLine() : ?string
	{
		return $this->_typeLine;
	}
	
	/**
	 * Sets the uri of the related.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiRelatedCard
	 */
	public function setUri(?UriInterface $uri) : ScryfallApiRelatedCard
	{
		$this->_uri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiRelatedCardInterface::getUri()
	 */
	public function getUri() : ?UriInterface
	{
		return $this->_uri;
	}
	
}
