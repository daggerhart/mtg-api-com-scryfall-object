<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;

/**
 * ScryfallApiRuling class file.
 * 
 * This class represents a ruling about a card.
 * 
 * @author Anastaszor
 */
class ScryfallApiRuling implements ScryfallApiRulingInterface
{
	
	/**
	 * The id of the oracle object.
	 * 
	 * @var UuidInterface
	 */
	protected $_oracleId;
	
	/**
	 * The source of this ruling.
	 * 
	 * @var ?string
	 */
	protected $_source;
	
	/**
	 * The date of publication of this ruling.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected $_publishedAt;
	
	/**
	 * The text of this ruling.
	 * 
	 * @var ?string
	 */
	protected $_comment;
	
	/**
	 * Builds a new ScryfallApiRuling with its data.
	 * 
	 * @param UuidInterface $oracleId
	 */
	public function __construct(UuidInterface $oracleId)
	{
		$this->_oracleId = $oracleId;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL RULING '.$this->_oracleId->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiRulingInterface::getOracleId()
	 */
	public function getOracleId() : UuidInterface
	{
		return $this->_oracleId;
	}
	
	/**
	 * Sets the source of this ruling.
	 * 
	 * @param ?string $source
	 * @return ScryfallApiRuling
	 */
	public function setSource(?string $source) : ScryfallApiRuling
	{
		$this->_source = $source;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiRulingInterface::getSource()
	 */
	public function getSource() : ?string
	{
		return $this->_source;
	}
	
	/**
	 * Sets when this ruling was published.
	 * 
	 * @param ?DateTimeInterface $datetime
	 * @return ScryfallApiRuling
	 */
	public function setPublishedAt(?DateTimeInterface $datetime) : ScryfallApiRuling
	{
		$this->_publishedAt = $datetime;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiRulingInterface::getPublishedAt()
	 */
	public function getPublishedAt() : ?DateTimeInterface
	{
		return $this->_publishedAt;
	}
	
	/**
	 * Sets the comment of this ruling.
	 * 
	 * @param ?string $comment
	 * @return ScryfallApiRuling
	 */
	public function setComment(?string $comment) : ScryfallApiRuling
	{
		$this->_comment = $comment;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiRulingInterface::getComment()
	 */
	public function getComment() : ?string
	{
		return $this->_comment;
	}
	
}
