<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

/**
 * ScryfallApiPrices class file.
 * 
 * An object containing daily price information for this card, including usd,
 * usd_foil, eur, and tix prices, as strings.
 * 
 * @author Anastaszor
 */
class ScryfallApiPrices implements ScryfallApiPricesInterface
{
	
	/**
	 * The value of the card, in usd.
	 * 
	 * @var ?float
	 */
	protected $_usd;
	
	/**
	 * The value of the foil card, in usd.
	 * 
	 * @var ?float
	 */
	protected $_usdFoil;
	
	/**
	 * The value of the etched card, if exists, in usd.
	 * 
	 * @var ?float
	 */
	protected $_usdEtched;
	
	/**
	 * The value of the glossy card, if exists, in usd.
	 * 
	 * @var ?float
	 */
	protected $_usdGlossy;
	
	/**
	 * The value of the card, in eur.
	 * 
	 * @var ?float
	 */
	protected $_eur;
	
	/**
	 * The value of the card, in eur foil.
	 * 
	 * @var ?float
	 */
	protected $_eurFoil;
	
	/**
	 * The value of the card, in tix.
	 * 
	 * @var ?float
	 */
	protected $_tix;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL API PRICE';
	}
	
	/**
	 * Sets the usd value.
	 * 
	 * @param ?float $value
	 * @return ScryfallApiPrices
	 */
	public function setUsd(?float $value) : ScryfallApiPrices
	{
		$this->_usd = $value;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPricesInterface::getUsd()
	 */
	public function getUsd() : ?float
	{
		return $this->_usd;
	}
	
	/**
	 * Sets the usd foil value.
	 * 
	 * @param ?float $value
	 * @return ScryfallApiPrices
	 */
	public function setUsdFoil(?float $value) : ScryfallApiPrices
	{
		$this->_usdFoil = $value;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPricesInterface::getUsdFoil()
	 */
	public function getUsdFoil() : ?float
	{
		return $this->_usdFoil;
	}
	
	/**
	 * Sets the usd etched value.
	 * 
	 * @param ?float $value
	 * @return ScryfallApiPrices
	 */
	public function setUsdEtched(?float $value) : ScryfallApiPrices
	{
		$this->_usdEtched = $value;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPricesInterface::getUsdEtched()
	 */
	public function getUsdEtched() : ?float
	{
		return $this->_usdEtched;
	}
	
	/**
	 * Sets the usd glossy value.
	 * 
	 * @param ?float $value
	 * @return ScryfallApiPrices
	 */
	public function setUsdGlossy(?float $value) : ScryfallApiPrices
	{
		$this->_usdGlossy = $value;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPricesInterface::getUsdGlossy()
	 */
	public function getUsdGlossy() : ?float
	{
		return $this->_usdGlossy;
	}
	
	/**
	 * Sets the eur value.
	 * 
	 * @param ?float $value
	 * @return ScryfallApiPrices
	 */
	public function setEur(?float $value) : ScryfallApiPrices
	{
		$this->_eur = $value;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPricesInterface::getEur()
	 */
	public function getEur() : ?float
	{
		return $this->_eur;
	}
	
	/**
	 * Sets the eur foil value.
	 * 
	 * @param ?float $value
	 * @return ScryfallApiPrices
	 */
	public function setEurFoil(?float $value) : ScryfallApiPrices
	{
		$this->_eurFoil = $value;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPricesInterface::getEurFoil()
	 */
	public function getEurFoil() : ?float
	{
		return $this->_eurFoil;
	}
	
	/**
	 * Sets the tix value.
	 * 
	 * @param ?float $value
	 * @return ScryfallApiPrices
	 */
	public function setTix(?float $value) : ScryfallApiPrices
	{
		$this->_tix = $value;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiPricesInterface::getTix()
	 */
	public function getTix() : ?float
	{
		return $this->_tix;
	}
	
}
