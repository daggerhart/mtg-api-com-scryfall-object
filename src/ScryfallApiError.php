<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use PhpExtended\Endpoint\HttpJsonErrorInterface;

/**
 * ScryfallApiObject class file.
 * 
 * This class is a generic object for all classes of this api.
 * 
 * @author Anastaszor
 */
class ScryfallApiError implements HttpJsonErrorInterface, ScryfallApiErrorInterface
{
	
	/**
	 * The detailed error.
	 * 
	 * @var ?string
	 */
	protected $_details;
	
	/**
	 * The error http code.
	 * 
	 * @var ?integer
	 */
	protected $_status;
	
	/**
	 * The type error.
	 * 
	 * @var ?string
	 */
	protected $_type;
	
	/**
	 * The warnings for this object.
	 * 
	 * @var array<integer, string>
	 */
	protected $_warnings = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL ERROR';
	}
	
	/**
	 * Sets the details of this error.
	 * 
	 * @param ?string $details
	 * @return ScryfallApiError
	 */
	public function setDetails(?string $details) : ScryfallApiError
	{
		$this->_details = $details;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiErrorInterface::getDetails()
	 */
	public function getDetails() : ?string
	{
		return $this->_details;
	}
	
	/**
	 * Sets the status of this error.
	 * 
	 * @param ?integer $status
	 * @return ScryfallApiError
	 */
	public function setStatus(?int $status) : ScryfallApiError
	{
		$this->_status = $status;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiErrorInterface::getStatus()
	 */
	public function getStatus() : ?int
	{
		return $this->_status;
	}
	
	/**
	 * Sets the error type.
	 * 
	 * @param ?string $type
	 * @return ScryfallApiError
	 */
	public function setType(?string $type) : ScryfallApiError
	{
		$this->_type = $type;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiErrorInterface::getType()
	 */
	public function getType() : ?string
	{
		return $this->_type;
	}
	
	/**
	 * Sets the warnings of this error.
	 * 
	 * @param array<integer, string> $warnings
	 * @return ScryfallApiError
	 */
	public function setWarnings(array $warnings) : ScryfallApiError
	{
		$this->_warnings = $warnings;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiErrorInterface::getWarnings()
	 */
	public function getWarnings() : array
	{
		return $this->_warnings;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Endpoint\HttpJsonErrorInterface::getErrorType()
	 */
	public function getErrorType() : ?string
	{
		return $this->getType();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Endpoint\HttpJsonErrorInterface::getErrorCode()
	 */
	public function getErrorCode() : ?int
	{
		return $this->getStatus();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Endpoint\HttpJsonErrorInterface::getErrorMessage()
	 */
	public function getErrorMessage() : ?string
	{
		return $this->getDetails();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Endpoint\HttpJsonErrorInterface::getErrorAdditionalInformations()
	 */
	public function getErrorAdditionalInformations() : array
	{
		return $this->getWarnings();
	}
	
}
