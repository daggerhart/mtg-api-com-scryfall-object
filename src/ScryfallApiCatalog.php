<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-scryfall-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Scryfall;

use Psr\Http\Message\UriInterface;

/**
 * ScryfallApiCatalog class file.
 * 
 * This class represents a catalog of terms.
 * 
 * @author Anastaszor
 */
class ScryfallApiCatalog implements ScryfallApiCatalogInterface
{
	
	/**
	 * The uri of current catalog.
	 * 
	 * @var ?UriInterface
	 */
	protected $_uri;
	
	/**
	 * The total values of this catalog.
	 * 
	 * @var ?integer
	 */
	protected $_totalValues;
	
	/**
	 * The data of this catalog.
	 * 
	 * @var array<integer, string>
	 */
	protected $_data = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return 'SCRYFALL CATALOG';
	}
	
	/**
	 * Sets the uri of this catalog.
	 * 
	 * @param ?UriInterface $uri
	 * @return ScryfallApiCatalog
	 */
	public function setUri(?UriInterface $uri) : ScryfallApiCatalog
	{
		$this->_uri = $uri;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCatalogInterface::getUri()
	 */
	public function getUri() : ?UriInterface
	{
		return $this->_uri;
	}
	
	/**
	 * Sets the total number of values.
	 * 
	 * @param ?integer $total
	 * @return ScryfallApiCatalog
	 */
	public function setTotalValues(?int $total) : ScryfallApiCatalog
	{
		$this->_totalValues = $total;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCatalogInterface::getTotalValues()
	 */
	public function getTotalValues() : ?int
	{
		return $this->_totalValues;
	}
	
	/**
	 * Sets the data of this catalog.
	 * 
	 * @param array<integer, string> $data
	 * @return ScryfallApiCatalog
	 */
	public function setData(array $data) : ScryfallApiCatalog
	{
		$this->_data = $data;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpMtg\Scryfall\ScryfallApiCatalogInterface::getData()
	 */
	public function getData() : array
	{
		return $this->_data;
	}
	
}
