# php-mtg/mtg-api-com-scryfall-object

A library that implements the php-mtg/mtg-api-com-scryfall-interface.

![coverage](https://gitlab.com/php-mtg/mtg-api-com-scryfall-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-mtg/mtg-api-com-scryfall-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer.
Download `composer.phar` from [their website](https://getcomposer.org/download/).
Then add to your composer.json :

```json
	"require": {
		...
		"php-mtg/mtg-api-com-scryfall-object": "^3",
		...
	}
```

Then run `php composer.phar update` to install this library.
The autoloading of all classes of this library is made through composer's autoloader.


## Basic Usage

This library may be used the following way :

```php

use PhpMtg\Scryfall\ScryfallApiEndpoint;

/* @var $client \Psr\Http\Client\ClientInterface */

$endpoint = new ScryfallApiEndpoint($client);

$collection = $this->_endpoint->getSets();

foreach($collection as $setResume)
{
	$page = 1;
	do
	{
		/** @var \PhpMtg\Scryfall\ScryfallApiPagination $pagination */
		$pagination = $this->_endpoint->getCardsSearch($page, 'e='.$setResume->getId());
		
		foreach($pagination->getData() as $card)
		{
			// do something with $card
		}
		
		$page++;
	}
	while($pagination->hasMore());
}

```


## License

MIT (See [license file](LICENSE)).
